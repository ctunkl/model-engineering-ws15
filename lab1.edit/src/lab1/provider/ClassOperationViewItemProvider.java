/**
 */
package lab1.provider;


import java.util.Collection;
import java.util.List;

import lab1.ClassOperationView;
import lab1.Lab1Factory;
import lab1.Lab1Package;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link lab1.ClassOperationView} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassOperationViewItemProvider extends ViewItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassOperationViewItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLayoutPropertyDescriptor(object);
			addViewelementsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Layout feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLayoutPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ClassOperationView_layout_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ClassOperationView_layout_feature", "_UI_ClassOperationView_type"),
				 Lab1Package.Literals.CLASS_OPERATION_VIEW__LAYOUT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Viewelements feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addViewelementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ClassOperationView_viewelements_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ClassOperationView_viewelements_feature", "_UI_ClassOperationView_type"),
				 Lab1Package.Literals.CLASS_OPERATION_VIEW__VIEWELEMENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(Lab1Package.Literals.CLASS_OPERATION_VIEW__VIEWELEMENTS);
			childrenFeatures.add(Lab1Package.Literals.CLASS_OPERATION_VIEW__ELEMENTGROUPS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ClassOperationView.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ClassOperationView"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ClassOperationView)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ClassOperationView_type") :
			getString("_UI_ClassOperationView_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ClassOperationView.class)) {
			case Lab1Package.CLASS_OPERATION_VIEW__LAYOUT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case Lab1Package.CLASS_OPERATION_VIEW__VIEWELEMENTS:
			case Lab1Package.CLASS_OPERATION_VIEW__ELEMENTGROUPS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(Lab1Package.Literals.CLASS_OPERATION_VIEW__VIEWELEMENTS,
				 Lab1Factory.eINSTANCE.createDateTimePicker()));

		newChildDescriptors.add
			(createChildParameter
				(Lab1Package.Literals.CLASS_OPERATION_VIEW__VIEWELEMENTS,
				 Lab1Factory.eINSTANCE.createSelection()));

		newChildDescriptors.add
			(createChildParameter
				(Lab1Package.Literals.CLASS_OPERATION_VIEW__VIEWELEMENTS,
				 Lab1Factory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(Lab1Package.Literals.CLASS_OPERATION_VIEW__VIEWELEMENTS,
				 Lab1Factory.eINSTANCE.createList()));

		newChildDescriptors.add
			(createChildParameter
				(Lab1Package.Literals.CLASS_OPERATION_VIEW__VIEWELEMENTS,
				 Lab1Factory.eINSTANCE.createTable()));

		newChildDescriptors.add
			(createChildParameter
				(Lab1Package.Literals.CLASS_OPERATION_VIEW__ELEMENTGROUPS,
				 Lab1Factory.eINSTANCE.createElementGroup()));
	}

}
