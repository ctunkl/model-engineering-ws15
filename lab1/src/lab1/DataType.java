/**
 */
package lab1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lab1.Lab1Package#getDataType()
 * @model
 * @generated
 */
public interface DataType extends ClassPropertyType {
} // DataType
