/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visibility Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.VisibilityCondition#getConditionID <em>Condition ID</em>}</li>
 *   <li>{@link lab1.VisibilityCondition#getVisibilityType <em>Visibility Type</em>}</li>
 *   <li>{@link lab1.VisibilityCondition#getCompositecondition <em>Compositecondition</em>}</li>
 *   <li>{@link lab1.VisibilityCondition#getChildren <em>Children</em>}</li>
 *   <li>{@link lab1.VisibilityCondition#getViewelement <em>Viewelement</em>}</li>
 *   <li>{@link lab1.VisibilityCondition#getElementgroup <em>Elementgroup</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getVisibilityCondition()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='comparisonsOnlyWithPropertiesFromSameView_viewElements uniqueId sameTypeForChildren'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot comparisonsOnlyWithPropertiesFromSameView_viewElements='children -> \n\t\t\tselect(c | c.oclIsKindOf(ComparisonCondition)) -> collect(c | c.oclAsType(ComparisonCondition)) -> \n\t\t\tforAll(c | ViewElement.allInstances() -> select(ve | ve.visibilitycondition -> includes(self)) -> \n\t\t\t\t       forAll(ve2 | ve2.view = c.propertyelement.view)\n\t\t\t)' uniqueId='not VisibilityCondition.allInstances() -> exists(c | c <> self and c.conditionID = self.conditionID)' sameTypeForChildren='children -> forAll(c | c.visibilityType = self.visibilityType)'"
 * @generated
 */
public interface VisibilityCondition extends EObject {
	/**
	 * Returns the value of the '<em><b>Condition ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition ID</em>' attribute.
	 * @see #setConditionID(int)
	 * @see lab1.Lab1Package#getVisibilityCondition_ConditionID()
	 * @model
	 * @generated
	 */
	int getConditionID();

	/**
	 * Sets the value of the '{@link lab1.VisibilityCondition#getConditionID <em>Condition ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition ID</em>' attribute.
	 * @see #getConditionID()
	 * @generated
	 */
	void setConditionID(int value);

	/**
	 * Returns the value of the '<em><b>Visibility Type</b></em>' attribute.
	 * The default value is <code>"SHOWN"</code>.
	 * The literals are from the enumeration {@link lab1.Visibility}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility Type</em>' attribute.
	 * @see lab1.Visibility
	 * @see #setVisibilityType(Visibility)
	 * @see lab1.Lab1Package#getVisibilityCondition_VisibilityType()
	 * @model default="SHOWN"
	 * @generated
	 */
	Visibility getVisibilityType();

	/**
	 * Sets the value of the '{@link lab1.VisibilityCondition#getVisibilityType <em>Visibility Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility Type</em>' attribute.
	 * @see lab1.Visibility
	 * @see #getVisibilityType()
	 * @generated
	 */
	void setVisibilityType(Visibility value);

	/**
	 * Returns the value of the '<em><b>Compositecondition</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link lab1.CompositeCondition#getOperands <em>Operands</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compositecondition</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compositecondition</em>' container reference.
	 * @see #setCompositecondition(CompositeCondition)
	 * @see lab1.Lab1Package#getVisibilityCondition_Compositecondition()
	 * @see lab1.CompositeCondition#getOperands
	 * @model opposite="operands" transient="false"
	 * @generated
	 */
	CompositeCondition getCompositecondition();

	/**
	 * Sets the value of the '{@link lab1.VisibilityCondition#getCompositecondition <em>Compositecondition</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compositecondition</em>' container reference.
	 * @see #getCompositecondition()
	 * @generated
	 */
	void setCompositecondition(CompositeCondition value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' reference list.
	 * The list contents are of type {@link lab1.VisibilityCondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' reference list.
	 * @see lab1.Lab1Package#getVisibilityCondition_Children()
	 * @model derived="true" ordered="false"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='self -> closure(p | p -> select(c | c.oclIsKindOf(CompositeCondition)) -> collect(c | c.oclAsType(CompositeCondition)).operands)'"
	 * @generated
	 */
	EList<VisibilityCondition> getChildren();

	/**
	 * Returns the value of the '<em><b>Viewelement</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link lab1.ViewElement#getVisibilitycondition <em>Visibilitycondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewelement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewelement</em>' reference.
	 * @see #setViewelement(ViewElement)
	 * @see lab1.Lab1Package#getVisibilityCondition_Viewelement()
	 * @see lab1.ViewElement#getVisibilitycondition
	 * @model opposite="visibilitycondition"
	 * @generated
	 */
	ViewElement getViewelement();

	/**
	 * Sets the value of the '{@link lab1.VisibilityCondition#getViewelement <em>Viewelement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Viewelement</em>' reference.
	 * @see #getViewelement()
	 * @generated
	 */
	void setViewelement(ViewElement value);

	/**
	 * Returns the value of the '<em><b>Elementgroup</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link lab1.ElementGroup#getVisibilitycondition <em>Visibilitycondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elementgroup</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elementgroup</em>' reference.
	 * @see #setElementgroup(ElementGroup)
	 * @see lab1.Lab1Package#getVisibilityCondition_Elementgroup()
	 * @see lab1.ElementGroup#getVisibilitycondition
	 * @model opposite="visibilitycondition"
	 * @generated
	 */
	ElementGroup getElementgroup();

	/**
	 * Sets the value of the '{@link lab1.VisibilityCondition#getElementgroup <em>Elementgroup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elementgroup</em>' reference.
	 * @see #getElementgroup()
	 * @generated
	 */
	void setElementgroup(ElementGroup value);

} // VisibilityCondition
