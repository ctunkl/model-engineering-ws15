/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>View Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.ViewGroup#getName <em>Name</em>}</li>
 *   <li>{@link lab1.ViewGroup#getViews <em>Views</em>}</li>
 *   <li>{@link lab1.ViewGroup#getStartView <em>Start View</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getViewGroup()
 * @model
 * @generated
 */
public interface ViewGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see lab1.Lab1Package#getViewGroup_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link lab1.ViewGroup#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Views</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.View}.
	 * It is bidirectional and its opposite is '{@link lab1.View#getViewgroup <em>Viewgroup</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Views</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Views</em>' containment reference list.
	 * @see lab1.Lab1Package#getViewGroup_Views()
	 * @see lab1.View#getViewgroup
	 * @model opposite="viewgroup" containment="true" required="true"
	 * @generated
	 */
	EList<View> getViews();

	/**
	 * Returns the value of the '<em><b>Start View</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start View</em>' reference.
	 * @see #setStartView(View)
	 * @see lab1.Lab1Package#getViewGroup_StartView()
	 * @model required="true"
	 * @generated
	 */
	View getStartView();

	/**
	 * Sets the value of the '{@link lab1.ViewGroup#getStartView <em>Start View</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start View</em>' reference.
	 * @see #getStartView()
	 * @generated
	 */
	void setStartView(View value);

} // ViewGroup
