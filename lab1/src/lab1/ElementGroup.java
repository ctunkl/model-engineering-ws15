/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.ElementGroup#getHeader <em>Header</em>}</li>
 *   <li>{@link lab1.ElementGroup#getLayout <em>Layout</em>}</li>
 *   <li>{@link lab1.ElementGroup#getVisibilitycondition <em>Visibilitycondition</em>}</li>
 *   <li>{@link lab1.ElementGroup#getViewelements <em>Viewelements</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getElementGroup()
 * @model
 * @generated
 */
public interface ElementGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Header</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header</em>' attribute.
	 * @see #setHeader(String)
	 * @see lab1.Lab1Package#getElementGroup_Header()
	 * @model
	 * @generated
	 */
	String getHeader();

	/**
	 * Sets the value of the '{@link lab1.ElementGroup#getHeader <em>Header</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header</em>' attribute.
	 * @see #getHeader()
	 * @generated
	 */
	void setHeader(String value);

	/**
	 * Returns the value of the '<em><b>Layout</b></em>' attribute.
	 * The default value is <code>"VERTICAL"</code>.
	 * The literals are from the enumeration {@link lab1.Layout}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout</em>' attribute.
	 * @see lab1.Layout
	 * @see #setLayout(Layout)
	 * @see lab1.Lab1Package#getElementGroup_Layout()
	 * @model default="VERTICAL"
	 * @generated
	 */
	Layout getLayout();

	/**
	 * Sets the value of the '{@link lab1.ElementGroup#getLayout <em>Layout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layout</em>' attribute.
	 * @see lab1.Layout
	 * @see #getLayout()
	 * @generated
	 */
	void setLayout(Layout value);

	/**
	 * Returns the value of the '<em><b>Visibilitycondition</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link lab1.VisibilityCondition#getElementgroup <em>Elementgroup</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibilitycondition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibilitycondition</em>' reference.
	 * @see #setVisibilitycondition(VisibilityCondition)
	 * @see lab1.Lab1Package#getElementGroup_Visibilitycondition()
	 * @see lab1.VisibilityCondition#getElementgroup
	 * @model opposite="elementgroup"
	 * @generated
	 */
	VisibilityCondition getVisibilitycondition();

	/**
	 * Sets the value of the '{@link lab1.ElementGroup#getVisibilitycondition <em>Visibilitycondition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibilitycondition</em>' reference.
	 * @see #getVisibilitycondition()
	 * @generated
	 */
	void setVisibilitycondition(VisibilityCondition value);

	/**
	 * Returns the value of the '<em><b>Viewelements</b></em>' reference list.
	 * The list contents are of type {@link lab1.ViewElement}.
	 * It is bidirectional and its opposite is '{@link lab1.ViewElement#getElementgroup <em>Elementgroup</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewelements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewelements</em>' reference list.
	 * @see lab1.Lab1Package#getElementGroup_Viewelements()
	 * @see lab1.ViewElement#getElementgroup
	 * @model opposite="elementgroup"
	 * @generated
	 */
	EList<ViewElement> getViewelements();

} // ElementGroup
