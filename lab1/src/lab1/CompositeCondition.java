/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.CompositeCondition#getOperator <em>Operator</em>}</li>
 *   <li>{@link lab1.CompositeCondition#getOperands <em>Operands</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getCompositeCondition()
 * @model
 * @generated
 */
public interface CompositeCondition extends VisibilityCondition {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link lab1.CompositeConditionOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see lab1.CompositeConditionOperator
	 * @see #setOperator(CompositeConditionOperator)
	 * @see lab1.Lab1Package#getCompositeCondition_Operator()
	 * @model
	 * @generated
	 */
	CompositeConditionOperator getOperator();

	/**
	 * Sets the value of the '{@link lab1.CompositeCondition#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see lab1.CompositeConditionOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(CompositeConditionOperator value);

	/**
	 * Returns the value of the '<em><b>Operands</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.VisibilityCondition}.
	 * It is bidirectional and its opposite is '{@link lab1.VisibilityCondition#getCompositecondition <em>Compositecondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operands</em>' containment reference list.
	 * @see lab1.Lab1Package#getCompositeCondition_Operands()
	 * @see lab1.VisibilityCondition#getCompositecondition
	 * @model opposite="compositecondition" containment="true" lower="2" upper="2"
	 * @generated
	 */
	EList<VisibilityCondition> getOperands();

} // CompositeCondition
