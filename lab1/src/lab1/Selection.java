/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Selection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.Selection#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getSelection()
 * @model
 * @generated
 */
public interface Selection extends PropertyElement {
	/**
	 * Returns the value of the '<em><b>Values</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.SelectionItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' containment reference list.
	 * @see lab1.Lab1Package#getSelection_Values()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<SelectionItem> getValues();

} // Selection
