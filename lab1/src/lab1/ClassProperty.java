/**
 */
package lab1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.ClassProperty#isIsId <em>Is Id</em>}</li>
 *   <li>{@link lab1.ClassProperty#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getClassProperty()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='upperIsOne idIsMandatory otherPropertiesAreOptional'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot upperIsOne='self.upper = 1' idIsMandatory='isId implies lower = 1' otherPropertiesAreOptional='(not isId) implies (lower = 0 or lower = 1)'"
 * @generated
 */
public interface ClassProperty extends Property {
	/**
	 * Returns the value of the '<em><b>Is Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Id</em>' attribute.
	 * @see #setIsId(boolean)
	 * @see lab1.Lab1Package#getClassProperty_IsId()
	 * @model
	 * @generated
	 */
	boolean isIsId();

	/**
	 * Sets the value of the '{@link lab1.ClassProperty#isIsId <em>Is Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Id</em>' attribute.
	 * @see #isIsId()
	 * @generated
	 */
	void setIsId(boolean value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(ClassPropertyType)
	 * @see lab1.Lab1Package#getClassProperty_Type()
	 * @model required="true"
	 * @generated
	 */
	ClassPropertyType getType();

	/**
	 * Sets the value of the '{@link lab1.ClassProperty#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ClassPropertyType value);

} // ClassProperty
