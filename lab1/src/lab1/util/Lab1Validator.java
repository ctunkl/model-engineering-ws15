/**
 */
package lab1.util;

import java.util.Map;

import lab1.Association;
import lab1.AssociationElement;
import lab1.AssociationProperty;
import lab1.ClassIndexView;
import lab1.ClassOperationView;
import lab1.ClassProperty;
import lab1.ClassPropertyType;
import lab1.Column;
import lab1.ComparisonCondition;
import lab1.ComparisonConditionOperator;
import lab1.CompositeCondition;
import lab1.CompositeConditionOperator;
import lab1.DataType;
import lab1.DateTimePicker;
import lab1.DomainModel;
import lab1.ElementGroup;
import lab1.EnumerationLiteral;
import lab1.EnumerationLiteralItem;
import lab1.EnumerationType;
import lab1.Lab1Package;
import lab1.Layout;
import lab1.Link;
import lab1.List;
import lab1.Property;
import lab1.PropertyElement;
import lab1.Selection;
import lab1.SelectionItem;
import lab1.Table;
import lab1.Text;
import lab1.View;
import lab1.ViewElement;
import lab1.ViewGroup;
import lab1.ViewModel;
import lab1.Visibility;
import lab1.VisibilityCondition;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see lab1.Lab1Package
 * @generated
 */
public class Lab1Validator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final Lab1Validator INSTANCE = new Lab1Validator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "lab1";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lab1Validator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return Lab1Package.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case Lab1Package.DOMAIN_MODEL:
				return validateDomainModel((DomainModel)value, diagnostics, context);
			case Lab1Package.CLASS:
				return validateClass((lab1.Class)value, diagnostics, context);
			case Lab1Package.ASSOCIATION:
				return validateAssociation((Association)value, diagnostics, context);
			case Lab1Package.DATA_TYPE:
				return validateDataType((DataType)value, diagnostics, context);
			case Lab1Package.ENUMERATION_LITERAL:
				return validateEnumerationLiteral((EnumerationLiteral)value, diagnostics, context);
			case Lab1Package.ENUMERATION_TYPE:
				return validateEnumerationType((EnumerationType)value, diagnostics, context);
			case Lab1Package.CLASS_PROPERTY:
				return validateClassProperty((ClassProperty)value, diagnostics, context);
			case Lab1Package.CLASS_PROPERTY_TYPE:
				return validateClassPropertyType((ClassPropertyType)value, diagnostics, context);
			case Lab1Package.ASSOCIATION_PROPERTY:
				return validateAssociationProperty((AssociationProperty)value, diagnostics, context);
			case Lab1Package.PROPERTY:
				return validateProperty((Property)value, diagnostics, context);
			case Lab1Package.DATE_TIME_PICKER:
				return validateDateTimePicker((DateTimePicker)value, diagnostics, context);
			case Lab1Package.ASSOCIATION_ELEMENT:
				return validateAssociationElement((AssociationElement)value, diagnostics, context);
			case Lab1Package.VIEW_GROUP:
				return validateViewGroup((ViewGroup)value, diagnostics, context);
			case Lab1Package.SELECTION:
				return validateSelection((Selection)value, diagnostics, context);
			case Lab1Package.SELECTION_ITEM:
				return validateSelectionItem((SelectionItem)value, diagnostics, context);
			case Lab1Package.TEXT:
				return validateText((Text)value, diagnostics, context);
			case Lab1Package.VIEW_ELEMENT:
				return validateViewElement((ViewElement)value, diagnostics, context);
			case Lab1Package.CLASS_OPERATION_VIEW:
				return validateClassOperationView((ClassOperationView)value, diagnostics, context);
			case Lab1Package.VIEW:
				return validateView((View)value, diagnostics, context);
			case Lab1Package.CLASS_INDEX_VIEW:
				return validateClassIndexView((ClassIndexView)value, diagnostics, context);
			case Lab1Package.ELEMENT_GROUP:
				return validateElementGroup((ElementGroup)value, diagnostics, context);
			case Lab1Package.VIEW_MODEL:
				return validateViewModel((ViewModel)value, diagnostics, context);
			case Lab1Package.LIST:
				return validateList((List)value, diagnostics, context);
			case Lab1Package.TABLE:
				return validateTable((Table)value, diagnostics, context);
			case Lab1Package.COLUMN:
				return validateColumn((Column)value, diagnostics, context);
			case Lab1Package.VISIBILITY_CONDITION:
				return validateVisibilityCondition((VisibilityCondition)value, diagnostics, context);
			case Lab1Package.COMPARISON_CONDITION:
				return validateComparisonCondition((ComparisonCondition)value, diagnostics, context);
			case Lab1Package.COMPOSITE_CONDITION:
				return validateCompositeCondition((CompositeCondition)value, diagnostics, context);
			case Lab1Package.ENUMERATION_LITERAL_ITEM:
				return validateEnumerationLiteralItem((EnumerationLiteralItem)value, diagnostics, context);
			case Lab1Package.LINK:
				return validateLink((Link)value, diagnostics, context);
			case Lab1Package.PROPERTY_ELEMENT:
				return validatePropertyElement((PropertyElement)value, diagnostics, context);
			case Lab1Package.VISIBILITY:
				return validateVisibility((Visibility)value, diagnostics, context);
			case Lab1Package.COMPARISON_CONDITION_OPERATOR:
				return validateComparisonConditionOperator((ComparisonConditionOperator)value, diagnostics, context);
			case Lab1Package.COMPOSITE_CONDITION_OPERATOR:
				return validateCompositeConditionOperator((CompositeConditionOperator)value, diagnostics, context);
			case Lab1Package.LAYOUT:
				return validateLayout((Layout)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDomainModel(DomainModel domainModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(domainModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClass(lab1.Class class_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(class_, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validateClass_noCyclicInheritance(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validateClass_mustHaveExactlyOneId(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validateClass_uniquePropertyName(class_, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the noCyclicInheritance constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS__NO_CYCLIC_INHERITANCE__EEXPRESSION = "Class.allInstances() -> forAll(c: Class | self <> c implies (self.superclasses <> c.superclasses))";

	/**
	 * Validates the noCyclicInheritance constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClass_noCyclicInheritance(lab1.Class class_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.CLASS,
				 class_,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "noCyclicInheritance",
				 CLASS__NO_CYCLIC_INHERITANCE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the mustHaveExactlyOneId constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS__MUST_HAVE_EXACTLY_ONE_ID__EEXPRESSION = "self.superclasses.properties -> select(p | p.isId) -> size() = 1";

	/**
	 * Validates the mustHaveExactlyOneId constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClass_mustHaveExactlyOneId(lab1.Class class_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.CLASS,
				 class_,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "mustHaveExactlyOneId",
				 CLASS__MUST_HAVE_EXACTLY_ONE_ID__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the uniquePropertyName constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS__UNIQUE_PROPERTY_NAME__EEXPRESSION = "self.superclasses.properties -> isUnique(name)";

	/**
	 * Validates the uniquePropertyName constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClass_uniquePropertyName(lab1.Class class_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.CLASS,
				 class_,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "uniquePropertyName",
				 CLASS__UNIQUE_PROPERTY_NAME__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociation(Association association, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(association, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(association, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(association, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(association, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(association, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(association, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(association, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(association, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(association, diagnostics, context);
		if (result || diagnostics != null) result &= validateAssociation_exactlyOneNavigableEnd(association, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the exactlyOneNavigableEnd constraint of '<em>Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ASSOCIATION__EXACTLY_ONE_NAVIGABLE_END__EEXPRESSION = "self.associationProperties -> select(ap: AssociationProperty | ap.isNavigable) -> size() = 1";

	/**
	 * Validates the exactlyOneNavigableEnd constraint of '<em>Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociation_exactlyOneNavigableEnd(Association association, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.ASSOCIATION,
				 association,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "exactlyOneNavigableEnd",
				 ASSOCIATION__EXACTLY_ONE_NAVIGABLE_END__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataType(DataType dataType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(dataType, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(dataType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(dataType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(dataType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(dataType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(dataType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(dataType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(dataType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(dataType, diagnostics, context);
		if (result || diagnostics != null) result &= validateClassPropertyType_uniqueName(dataType, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumerationLiteral(EnumerationLiteral enumerationLiteral, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumerationLiteral, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumerationType(EnumerationType enumerationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(enumerationType, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(enumerationType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(enumerationType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(enumerationType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(enumerationType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(enumerationType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(enumerationType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(enumerationType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(enumerationType, diagnostics, context);
		if (result || diagnostics != null) result &= validateClassPropertyType_uniqueName(enumerationType, diagnostics, context);
		if (result || diagnostics != null) result &= validateEnumerationType_uniqueLiterals(enumerationType, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the uniqueLiterals constraint of '<em>Enumeration Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ENUMERATION_TYPE__UNIQUE_LITERALS__EEXPRESSION = "self.enumerationLiterals -> isUnique(value)";

	/**
	 * Validates the uniqueLiterals constraint of '<em>Enumeration Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumerationType_uniqueLiterals(EnumerationType enumerationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.ENUMERATION_TYPE,
				 enumerationType,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "uniqueLiterals",
				 ENUMERATION_TYPE__UNIQUE_LITERALS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassProperty(ClassProperty classProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(classProperty, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(classProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(classProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(classProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(classProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(classProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(classProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(classProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(classProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validateClassProperty_upperIsOne(classProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validateClassProperty_idIsMandatory(classProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validateClassProperty_otherPropertiesAreOptional(classProperty, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the upperIsOne constraint of '<em>Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS_PROPERTY__UPPER_IS_ONE__EEXPRESSION = "self.upper = 1";

	/**
	 * Validates the upperIsOne constraint of '<em>Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassProperty_upperIsOne(ClassProperty classProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.CLASS_PROPERTY,
				 classProperty,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "upperIsOne",
				 CLASS_PROPERTY__UPPER_IS_ONE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the idIsMandatory constraint of '<em>Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS_PROPERTY__ID_IS_MANDATORY__EEXPRESSION = "isId implies lower = 1";

	/**
	 * Validates the idIsMandatory constraint of '<em>Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassProperty_idIsMandatory(ClassProperty classProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.CLASS_PROPERTY,
				 classProperty,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "idIsMandatory",
				 CLASS_PROPERTY__ID_IS_MANDATORY__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the otherPropertiesAreOptional constraint of '<em>Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS_PROPERTY__OTHER_PROPERTIES_ARE_OPTIONAL__EEXPRESSION = "(not isId) implies (lower = 0 or lower = 1)";

	/**
	 * Validates the otherPropertiesAreOptional constraint of '<em>Class Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassProperty_otherPropertiesAreOptional(ClassProperty classProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.CLASS_PROPERTY,
				 classProperty,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "otherPropertiesAreOptional",
				 CLASS_PROPERTY__OTHER_PROPERTIES_ARE_OPTIONAL__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassPropertyType(ClassPropertyType classPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(classPropertyType, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(classPropertyType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(classPropertyType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(classPropertyType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(classPropertyType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(classPropertyType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(classPropertyType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(classPropertyType, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(classPropertyType, diagnostics, context);
		if (result || diagnostics != null) result &= validateClassPropertyType_uniqueName(classPropertyType, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the uniqueName constraint of '<em>Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS_PROPERTY_TYPE__UNIQUE_NAME__EEXPRESSION = "ClassPropertyType.allInstances() -> select(e | self.name = e.name) -> size() = 1";

	/**
	 * Validates the uniqueName constraint of '<em>Class Property Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassPropertyType_uniqueName(ClassPropertyType classPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.CLASS_PROPERTY_TYPE,
				 classPropertyType,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "uniqueName",
				 CLASS_PROPERTY_TYPE__UNIQUE_NAME__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociationProperty(AssociationProperty associationProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(associationProperty, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(associationProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(associationProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(associationProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(associationProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(associationProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(associationProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(associationProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(associationProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validateAssociationProperty_validMultiplicities(associationProperty, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the validMultiplicities constraint of '<em>Association Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ASSOCIATION_PROPERTY__VALID_MULTIPLICITIES__EEXPRESSION = "lower >= 0 and (upper = (-1) or (upper > 0 and upper >= lower))";

	/**
	 * Validates the validMultiplicities constraint of '<em>Association Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociationProperty_validMultiplicities(AssociationProperty associationProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.ASSOCIATION_PROPERTY,
				 associationProperty,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "validMultiplicities",
				 ASSOCIATION_PROPERTY__VALID_MULTIPLICITIES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProperty(Property property, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(property, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDateTimePicker(DateTimePicker dateTimePicker, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dateTimePicker, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociationElement(AssociationElement associationElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(associationElement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewGroup(ViewGroup viewGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(viewGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSelection(Selection selection, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(selection, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSelectionItem(SelectionItem selectionItem, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(selectionItem, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateText(Text text, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(text, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewElement(ViewElement viewElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(viewElement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassOperationView(ClassOperationView classOperationView, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(classOperationView, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(classOperationView, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(classOperationView, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(classOperationView, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(classOperationView, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(classOperationView, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(classOperationView, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(classOperationView, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(classOperationView, diagnostics, context);
		if (result || diagnostics != null) result &= validateClassOperationView_sameClassForPropertyElements(classOperationView, diagnostics, context);
		if (result || diagnostics != null) result &= validateClassOperationView_sameClassForAssociationElements(classOperationView, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the sameClassForPropertyElements constraint of '<em>Class Operation View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS_OPERATION_VIEW__SAME_CLASS_FOR_PROPERTY_ELEMENTS__EEXPRESSION = "self.viewelements -> select(ele | ele.oclIsKindOf(PropertyElement)) ->\n" +
		"\t\t\t\t\tcollect(ele | ele.oclAsType(PropertyElement).classproperty) -> \n" +
		"\t\t\t\t\tforAll(prop: ClassProperty | \n" +
		"\t\t\t\t\t\tClass.allInstances() -> select(clazz | (clazz.properties -> includes(prop)) and instance.superclasses -> includes(clazz)) -> notEmpty())";

	/**
	 * Validates the sameClassForPropertyElements constraint of '<em>Class Operation View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassOperationView_sameClassForPropertyElements(ClassOperationView classOperationView, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.CLASS_OPERATION_VIEW,
				 classOperationView,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "sameClassForPropertyElements",
				 CLASS_OPERATION_VIEW__SAME_CLASS_FOR_PROPERTY_ELEMENTS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the sameClassForAssociationElements constraint of '<em>Class Operation View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS_OPERATION_VIEW__SAME_CLASS_FOR_ASSOCIATION_ELEMENTS__EEXPRESSION = "self.viewelements -> select(ele | ele.oclIsKindOf(AssociationElement)) ->\n" +
		"\t\t\t\t\tcollect(ele | ele.oclAsType(AssociationElement).association.associationProperties.oclAsType(AssociationProperty)) ->  \n" +
		"\t\t\t\t\tselect(prop | not prop.isNavigable) ->\n" +
		"\t\t\t\t\tforAll(prop | (Class.allInstances() -> select(clazz | (clazz.associationproperty -> includes(prop)) and instance.superclasses -> includes(clazz))) -> notEmpty())";

	/**
	 * Validates the sameClassForAssociationElements constraint of '<em>Class Operation View</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassOperationView_sameClassForAssociationElements(ClassOperationView classOperationView, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.CLASS_OPERATION_VIEW,
				 classOperationView,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "sameClassForAssociationElements",
				 CLASS_OPERATION_VIEW__SAME_CLASS_FOR_ASSOCIATION_ELEMENTS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateView(View view, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(view, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassIndexView(ClassIndexView classIndexView, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(classIndexView, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateElementGroup(ElementGroup elementGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(elementGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewModel(ViewModel viewModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(viewModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateList(List list, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(list, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTable(Table table, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(table, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateColumn(Column column, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(column, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVisibilityCondition(VisibilityCondition visibilityCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(visibilityCondition, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(visibilityCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(visibilityCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(visibilityCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(visibilityCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(visibilityCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(visibilityCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(visibilityCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(visibilityCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validateVisibilityCondition_comparisonsOnlyWithPropertiesFromSameView_viewElements(visibilityCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validateVisibilityCondition_uniqueId(visibilityCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validateVisibilityCondition_sameTypeForChildren(visibilityCondition, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the comparisonsOnlyWithPropertiesFromSameView_viewElements constraint of '<em>Visibility Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VISIBILITY_CONDITION__COMPARISONS_ONLY_WITH_PROPERTIES_FROM_SAME_VIEW_VIEW_ELEMENTS__EEXPRESSION = "children -> \n" +
		"\t\t\tselect(c | c.oclIsKindOf(ComparisonCondition)) -> collect(c | c.oclAsType(ComparisonCondition)) -> \n" +
		"\t\t\tforAll(c | ViewElement.allInstances() -> select(ve | ve.visibilitycondition -> includes(self)) -> \n" +
		"\t\t\t\t       forAll(ve2 | ve2.view = c.propertyelement.view)\n" +
		"\t\t\t)";

	/**
	 * Validates the comparisonsOnlyWithPropertiesFromSameView_viewElements constraint of '<em>Visibility Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVisibilityCondition_comparisonsOnlyWithPropertiesFromSameView_viewElements(VisibilityCondition visibilityCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.VISIBILITY_CONDITION,
				 visibilityCondition,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "comparisonsOnlyWithPropertiesFromSameView_viewElements",
				 VISIBILITY_CONDITION__COMPARISONS_ONLY_WITH_PROPERTIES_FROM_SAME_VIEW_VIEW_ELEMENTS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the uniqueId constraint of '<em>Visibility Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VISIBILITY_CONDITION__UNIQUE_ID__EEXPRESSION = "not VisibilityCondition.allInstances() -> exists(c | c <> self and c.conditionID = self.conditionID)";

	/**
	 * Validates the uniqueId constraint of '<em>Visibility Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVisibilityCondition_uniqueId(VisibilityCondition visibilityCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.VISIBILITY_CONDITION,
				 visibilityCondition,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "uniqueId",
				 VISIBILITY_CONDITION__UNIQUE_ID__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the sameTypeForChildren constraint of '<em>Visibility Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VISIBILITY_CONDITION__SAME_TYPE_FOR_CHILDREN__EEXPRESSION = "children -> forAll(c | c.visibilityType = self.visibilityType)";

	/**
	 * Validates the sameTypeForChildren constraint of '<em>Visibility Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVisibilityCondition_sameTypeForChildren(VisibilityCondition visibilityCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(Lab1Package.Literals.VISIBILITY_CONDITION,
				 visibilityCondition,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "sameTypeForChildren",
				 VISIBILITY_CONDITION__SAME_TYPE_FOR_CHILDREN__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComparisonCondition(ComparisonCondition comparisonCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(comparisonCondition, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(comparisonCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(comparisonCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(comparisonCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(comparisonCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(comparisonCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(comparisonCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(comparisonCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(comparisonCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validateVisibilityCondition_comparisonsOnlyWithPropertiesFromSameView_viewElements(comparisonCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validateVisibilityCondition_uniqueId(comparisonCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validateVisibilityCondition_sameTypeForChildren(comparisonCondition, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeCondition(CompositeCondition compositeCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(compositeCondition, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validateVisibilityCondition_comparisonsOnlyWithPropertiesFromSameView_viewElements(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validateVisibilityCondition_uniqueId(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validateVisibilityCondition_sameTypeForChildren(compositeCondition, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumerationLiteralItem(EnumerationLiteralItem enumerationLiteralItem, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumerationLiteralItem, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLink(Link link, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(link, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePropertyElement(PropertyElement propertyElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(propertyElement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVisibility(Visibility visibility, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComparisonConditionOperator(ComparisonConditionOperator comparisonConditionOperator, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeConditionOperator(CompositeConditionOperator compositeConditionOperator, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLayout(Layout layout, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //Lab1Validator
