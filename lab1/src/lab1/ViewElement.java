/**
 */
package lab1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>View Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.ViewElement#getLabel <em>Label</em>}</li>
 *   <li>{@link lab1.ViewElement#getElementID <em>Element ID</em>}</li>
 *   <li>{@link lab1.ViewElement#getView <em>View</em>}</li>
 *   <li>{@link lab1.ViewElement#getVisibilitycondition <em>Visibilitycondition</em>}</li>
 *   <li>{@link lab1.ViewElement#getElementgroup <em>Elementgroup</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getViewElement()
 * @model abstract="true"
 * @generated
 */
public interface ViewElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see lab1.Lab1Package#getViewElement_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link lab1.ViewElement#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Element ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element ID</em>' attribute.
	 * @see #setElementID(int)
	 * @see lab1.Lab1Package#getViewElement_ElementID()
	 * @model
	 * @generated
	 */
	int getElementID();

	/**
	 * Sets the value of the '{@link lab1.ViewElement#getElementID <em>Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element ID</em>' attribute.
	 * @see #getElementID()
	 * @generated
	 */
	void setElementID(int value);

	/**
	 * Returns the value of the '<em><b>View</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link lab1.ClassOperationView#getViewelements <em>Viewelements</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>View</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>View</em>' container reference.
	 * @see #setView(ClassOperationView)
	 * @see lab1.Lab1Package#getViewElement_View()
	 * @see lab1.ClassOperationView#getViewelements
	 * @model opposite="viewelements" required="true" transient="false"
	 * @generated
	 */
	ClassOperationView getView();

	/**
	 * Sets the value of the '{@link lab1.ViewElement#getView <em>View</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>View</em>' container reference.
	 * @see #getView()
	 * @generated
	 */
	void setView(ClassOperationView value);

	/**
	 * Returns the value of the '<em><b>Elementgroup</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link lab1.ElementGroup#getViewelements <em>Viewelements</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elementgroup</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elementgroup</em>' reference.
	 * @see #setElementgroup(ElementGroup)
	 * @see lab1.Lab1Package#getViewElement_Elementgroup()
	 * @see lab1.ElementGroup#getViewelements
	 * @model opposite="viewelements"
	 * @generated
	 */
	ElementGroup getElementgroup();

	/**
	 * Sets the value of the '{@link lab1.ViewElement#getElementgroup <em>Elementgroup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elementgroup</em>' reference.
	 * @see #getElementgroup()
	 * @generated
	 */
	void setElementgroup(ElementGroup value);

	/**
	 * Returns the value of the '<em><b>Visibilitycondition</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link lab1.VisibilityCondition#getViewelement <em>Viewelement</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibilitycondition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibilitycondition</em>' reference.
	 * @see #setVisibilitycondition(VisibilityCondition)
	 * @see lab1.Lab1Package#getViewElement_Visibilitycondition()
	 * @see lab1.VisibilityCondition#getViewelement
	 * @model opposite="viewelement"
	 * @generated
	 */
	VisibilityCondition getVisibilitycondition();

	/**
	 * Sets the value of the '{@link lab1.ViewElement#getVisibilitycondition <em>Visibilitycondition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibilitycondition</em>' reference.
	 * @see #getVisibilitycondition()
	 * @generated
	 */
	void setVisibilitycondition(VisibilityCondition value);

} // ViewElement
