/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Operation View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.ClassOperationView#getLayout <em>Layout</em>}</li>
 *   <li>{@link lab1.ClassOperationView#getViewelements <em>Viewelements</em>}</li>
 *   <li>{@link lab1.ClassOperationView#getElementgroups <em>Elementgroups</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getClassOperationView()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='sameClassForPropertyElements sameClassForAssociationElements'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot sameClassForPropertyElements='self.viewelements -> select(ele | ele.oclIsKindOf(PropertyElement)) ->\n\t\t\t\t\tcollect(ele | ele.oclAsType(PropertyElement).classproperty) -> \n\t\t\t\t\tforAll(prop: ClassProperty | \n\t\t\t\t\t\tClass.allInstances() -> select(clazz | (clazz.properties -> includes(prop)) and instance.superclasses -> includes(clazz)) -> notEmpty())' sameClassForAssociationElements='self.viewelements -> select(ele | ele.oclIsKindOf(AssociationElement)) ->\n\t\t\t\t\tcollect(ele | ele.oclAsType(AssociationElement).association.associationProperties.oclAsType(AssociationProperty)) ->  \n\t\t\t\t\tselect(prop | not prop.isNavigable) ->\n\t\t\t\t\tforAll(prop | (Class.allInstances() -> select(clazz | (clazz.associationproperty -> includes(prop)) and instance.superclasses -> includes(clazz))) -> notEmpty())'"
 * @generated
 */
public interface ClassOperationView extends View {
	/**
	 * Returns the value of the '<em><b>Layout</b></em>' attribute.
	 * The default value is <code>"VERTICAL"</code>.
	 * The literals are from the enumeration {@link lab1.Layout}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout</em>' attribute.
	 * @see lab1.Layout
	 * @see #setLayout(Layout)
	 * @see lab1.Lab1Package#getClassOperationView_Layout()
	 * @model default="VERTICAL"
	 * @generated
	 */
	Layout getLayout();

	/**
	 * Sets the value of the '{@link lab1.ClassOperationView#getLayout <em>Layout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layout</em>' attribute.
	 * @see lab1.Layout
	 * @see #getLayout()
	 * @generated
	 */
	void setLayout(Layout value);

	/**
	 * Returns the value of the '<em><b>Viewelements</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.ViewElement}.
	 * It is bidirectional and its opposite is '{@link lab1.ViewElement#getView <em>View</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewelements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewelements</em>' containment reference list.
	 * @see lab1.Lab1Package#getClassOperationView_Viewelements()
	 * @see lab1.ViewElement#getView
	 * @model opposite="view" containment="true"
	 * @generated
	 */
	EList<ViewElement> getViewelements();

	/**
	 * Returns the value of the '<em><b>Elementgroups</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.ElementGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elementgroups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elementgroups</em>' containment reference list.
	 * @see lab1.Lab1Package#getClassOperationView_Elementgroups()
	 * @model containment="true"
	 * @generated
	 */
	EList<ElementGroup> getElementgroups();

} // ClassOperationView
