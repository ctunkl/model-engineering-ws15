/**
 */
package lab1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.AssociationProperty#getClass_ <em>Class</em>}</li>
 *   <li>{@link lab1.AssociationProperty#isIsNavigable <em>Is Navigable</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getAssociationProperty()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validMultiplicities'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot validMultiplicities='lower >= 0 and (upper = (-1) or (upper > 0 and upper >= lower))'"
 * @generated
 */
public interface AssociationProperty extends Property {
	/**
	 * Returns the value of the '<em><b>Class</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link lab1.Class#getAssociationproperty <em>Associationproperty</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' reference.
	 * @see #setClass(lab1.Class)
	 * @see lab1.Lab1Package#getAssociationProperty_Class()
	 * @see lab1.Class#getAssociationproperty
	 * @model opposite="associationproperty" required="true"
	 * @generated
	 */
	lab1.Class getClass_();

	/**
	 * Sets the value of the '{@link lab1.AssociationProperty#getClass_ <em>Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' reference.
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(lab1.Class value);

	/**
	 * Returns the value of the '<em><b>Is Navigable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Navigable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Navigable</em>' attribute.
	 * @see #setIsNavigable(boolean)
	 * @see lab1.Lab1Package#getAssociationProperty_IsNavigable()
	 * @model
	 * @generated
	 */
	boolean isIsNavigable();

	/**
	 * Sets the value of the '{@link lab1.AssociationProperty#isIsNavigable <em>Is Navigable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Navigable</em>' attribute.
	 * @see #isIsNavigable()
	 * @generated
	 */
	void setIsNavigable(boolean value);

} // AssociationProperty
