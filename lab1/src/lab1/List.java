/**
 */
package lab1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lab1.Lab1Package#getList()
 * @model
 * @generated
 */
public interface List extends AssociationElement {
} // List
