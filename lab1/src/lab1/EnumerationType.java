/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumeration Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.EnumerationType#getEnumerationLiterals <em>Enumeration Literals</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getEnumerationType()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='uniqueLiterals'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot uniqueLiterals='self.enumerationLiterals -> isUnique(value)'"
 * @generated
 */
public interface EnumerationType extends ClassPropertyType {
	/**
	 * Returns the value of the '<em><b>Enumeration Literals</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.EnumerationLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literals</em>' containment reference list.
	 * @see lab1.Lab1Package#getEnumerationType_EnumerationLiterals()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<EnumerationLiteral> getEnumerationLiterals();

} // EnumerationType
