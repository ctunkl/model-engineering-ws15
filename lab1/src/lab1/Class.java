/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.Class#getSuperclass <em>Superclass</em>}</li>
 *   <li>{@link lab1.Class#getProperties <em>Properties</em>}</li>
 *   <li>{@link lab1.Class#getAssociationproperty <em>Associationproperty</em>}</li>
 *   <li>{@link lab1.Class#getName <em>Name</em>}</li>
 *   <li>{@link lab1.Class#getSuperclasses <em>Superclasses</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getClass_()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='noCyclicInheritance mustHaveExactlyOneId uniquePropertyName'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot noCyclicInheritance='Class.allInstances() -> forAll(c: Class | self <> c implies (self.superclasses <> c.superclasses))' mustHaveExactlyOneId='self.superclasses.properties -> select(p | p.isId) -> size() = 1' uniquePropertyName='self.superclasses.properties -> isUnique(name)'"
 * @generated
 */
public interface Class extends EObject {
	/**
	 * Returns the value of the '<em><b>Superclass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Superclass</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Superclass</em>' reference.
	 * @see #setSuperclass(Class)
	 * @see lab1.Lab1Package#getClass_Superclass()
	 * @model
	 * @generated
	 */
	Class getSuperclass();

	/**
	 * Sets the value of the '{@link lab1.Class#getSuperclass <em>Superclass</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Superclass</em>' reference.
	 * @see #getSuperclass()
	 * @generated
	 */
	void setSuperclass(Class value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.ClassProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see lab1.Lab1Package#getClass_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<ClassProperty> getProperties();

	/**
	 * Returns the value of the '<em><b>Associationproperty</b></em>' reference list.
	 * The list contents are of type {@link lab1.AssociationProperty}.
	 * It is bidirectional and its opposite is '{@link lab1.AssociationProperty#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associationproperty</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associationproperty</em>' reference list.
	 * @see lab1.Lab1Package#getClass_Associationproperty()
	 * @see lab1.AssociationProperty#getClass_
	 * @model opposite="class"
	 * @generated
	 */
	EList<AssociationProperty> getAssociationproperty();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see lab1.Lab1Package#getClass_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link lab1.Class#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Superclasses</b></em>' reference list.
	 * The list contents are of type {@link lab1.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Superclasses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Superclasses</em>' reference list.
	 * @see lab1.Lab1Package#getClass_Superclasses()
	 * @model derived="true" ordered="false"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='self -> closure(superclass)'"
	 * @generated
	 */
	EList<Class> getSuperclasses();

} // Class
