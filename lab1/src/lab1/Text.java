/**
 */
package lab1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Text</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.Text#isShortText <em>Short Text</em>}</li>
 *   <li>{@link lab1.Text#getFormat <em>Format</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getText()
 * @model
 * @generated
 */
public interface Text extends PropertyElement {
	/**
	 * Returns the value of the '<em><b>Short Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Short Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Short Text</em>' attribute.
	 * @see #setShortText(boolean)
	 * @see lab1.Lab1Package#getText_ShortText()
	 * @model
	 * @generated
	 */
	boolean isShortText();

	/**
	 * Sets the value of the '{@link lab1.Text#isShortText <em>Short Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Short Text</em>' attribute.
	 * @see #isShortText()
	 * @generated
	 */
	void setShortText(boolean value);

	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see #setFormat(String)
	 * @see lab1.Lab1Package#getText_Format()
	 * @model
	 * @generated
	 */
	String getFormat();

	/**
	 * Sets the value of the '{@link lab1.Text#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(String value);

} // Text
