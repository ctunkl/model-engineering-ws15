/**
 */
package lab1;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Index View</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see lab1.Lab1Package#getClassIndexView()
 * @model
 * @generated
 */
public interface ClassIndexView extends View {

} // ClassIndexView
