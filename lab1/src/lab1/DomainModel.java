/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Domain Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.DomainModel#getAssociation <em>Association</em>}</li>
 *   <li>{@link lab1.DomainModel#getClass_ <em>Class</em>}</li>
 *   <li>{@link lab1.DomainModel#getClasspropertytype <em>Classpropertytype</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getDomainModel()
 * @model
 * @generated
 */
public interface DomainModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Association</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.Association}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association</em>' containment reference list.
	 * @see lab1.Lab1Package#getDomainModel_Association()
	 * @model containment="true"
	 * @generated
	 */
	EList<Association> getAssociation();

	/**
	 * Returns the value of the '<em><b>Class</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' containment reference list.
	 * @see lab1.Lab1Package#getDomainModel_Class()
	 * @model containment="true"
	 * @generated
	 */
	EList<lab1.Class> getClass_();

	/**
	 * Returns the value of the '<em><b>Classpropertytype</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.ClassPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classpropertytype</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classpropertytype</em>' containment reference list.
	 * @see lab1.Lab1Package#getDomainModel_Classpropertytype()
	 * @model containment="true"
	 * @generated
	 */
	EList<ClassPropertyType> getClasspropertytype();

} // DomainModel
