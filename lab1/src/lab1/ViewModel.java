/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>View Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.ViewModel#getViewgroups <em>Viewgroups</em>}</li>
 *   <li>{@link lab1.ViewModel#getWelcomeViewGroup <em>Welcome View Group</em>}</li>
 *   <li>{@link lab1.ViewModel#getVisibilitycondition <em>Visibilitycondition</em>}</li>
 *   <li>{@link lab1.ViewModel#getDomainmodel <em>Domainmodel</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getViewModel()
 * @model
 * @generated
 */
public interface ViewModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Viewgroups</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.ViewGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewgroups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewgroups</em>' containment reference list.
	 * @see lab1.Lab1Package#getViewModel_Viewgroups()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ViewGroup> getViewgroups();

	/**
	 * Returns the value of the '<em><b>Welcome View Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Welcome View Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Welcome View Group</em>' reference.
	 * @see #setWelcomeViewGroup(ViewGroup)
	 * @see lab1.Lab1Package#getViewModel_WelcomeViewGroup()
	 * @model required="true"
	 * @generated
	 */
	ViewGroup getWelcomeViewGroup();

	/**
	 * Sets the value of the '{@link lab1.ViewModel#getWelcomeViewGroup <em>Welcome View Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Welcome View Group</em>' reference.
	 * @see #getWelcomeViewGroup()
	 * @generated
	 */
	void setWelcomeViewGroup(ViewGroup value);

	/**
	 * Returns the value of the '<em><b>Visibilitycondition</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.VisibilityCondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibilitycondition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibilitycondition</em>' containment reference list.
	 * @see lab1.Lab1Package#getViewModel_Visibilitycondition()
	 * @model containment="true"
	 * @generated
	 */
	EList<VisibilityCondition> getVisibilitycondition();

	/**
	 * Returns the value of the '<em><b>Domainmodel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domainmodel</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domainmodel</em>' containment reference.
	 * @see #setDomainmodel(DomainModel)
	 * @see lab1.Lab1Package#getViewModel_Domainmodel()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DomainModel getDomainmodel();

	/**
	 * Sets the value of the '{@link lab1.ViewModel#getDomainmodel <em>Domainmodel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domainmodel</em>' containment reference.
	 * @see #getDomainmodel()
	 * @generated
	 */
	void setDomainmodel(DomainModel value);

} // ViewModel
