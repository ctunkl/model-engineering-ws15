/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.PropertyElement#getClassproperty <em>Classproperty</em>}</li>
 *   <li>{@link lab1.PropertyElement#getConditions <em>Conditions</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getPropertyElement()
 * @model abstract="true"
 * @generated
 */
public interface PropertyElement extends ViewElement {
	/**
	 * Returns the value of the '<em><b>Classproperty</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classproperty</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classproperty</em>' reference.
	 * @see #setClassproperty(ClassProperty)
	 * @see lab1.Lab1Package#getPropertyElement_Classproperty()
	 * @model required="true"
	 * @generated
	 */
	ClassProperty getClassproperty();

	/**
	 * Sets the value of the '{@link lab1.PropertyElement#getClassproperty <em>Classproperty</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Classproperty</em>' reference.
	 * @see #getClassproperty()
	 * @generated
	 */
	void setClassproperty(ClassProperty value);

	/**
	 * Returns the value of the '<em><b>Conditions</b></em>' reference list.
	 * The list contents are of type {@link lab1.ComparisonCondition}.
	 * It is bidirectional and its opposite is '{@link lab1.ComparisonCondition#getPropertyelement <em>Propertyelement</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditions</em>' reference list.
	 * @see lab1.Lab1Package#getPropertyElement_Conditions()
	 * @see lab1.ComparisonCondition#getPropertyelement
	 * @model opposite="propertyelement"
	 * @generated
	 */
	EList<ComparisonCondition> getConditions();

} // PropertyElement
