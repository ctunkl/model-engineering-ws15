/**
 */
package lab1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comparison Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.ComparisonCondition#getOperator <em>Operator</em>}</li>
 *   <li>{@link lab1.ComparisonCondition#getValue <em>Value</em>}</li>
 *   <li>{@link lab1.ComparisonCondition#getPropertyelement <em>Propertyelement</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getComparisonCondition()
 * @model
 * @generated
 */
public interface ComparisonCondition extends VisibilityCondition {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The default value is <code>"EQ"</code>.
	 * The literals are from the enumeration {@link lab1.ComparisonConditionOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see lab1.ComparisonConditionOperator
	 * @see #setOperator(ComparisonConditionOperator)
	 * @see lab1.Lab1Package#getComparisonCondition_Operator()
	 * @model default="EQ"
	 * @generated
	 */
	ComparisonConditionOperator getOperator();

	/**
	 * Sets the value of the '{@link lab1.ComparisonCondition#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see lab1.ComparisonConditionOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(ComparisonConditionOperator value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see lab1.Lab1Package#getComparisonCondition_Value()
	 * @model
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link lab1.ComparisonCondition#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Propertyelement</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link lab1.PropertyElement#getConditions <em>Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Propertyelement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Propertyelement</em>' reference.
	 * @see #setPropertyelement(PropertyElement)
	 * @see lab1.Lab1Package#getComparisonCondition_Propertyelement()
	 * @see lab1.PropertyElement#getConditions
	 * @model opposite="conditions" required="true"
	 * @generated
	 */
	PropertyElement getPropertyelement();

	/**
	 * Sets the value of the '{@link lab1.ComparisonCondition#getPropertyelement <em>Propertyelement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Propertyelement</em>' reference.
	 * @see #getPropertyelement()
	 * @generated
	 */
	void setPropertyelement(PropertyElement value);

} // ComparisonCondition
