/**
 */
package lab1.impl;

import java.util.Collection;

import lab1.DomainModel;
import lab1.Lab1Package;
import lab1.ViewGroup;
import lab1.ViewModel;
import lab1.VisibilityCondition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>View Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lab1.impl.ViewModelImpl#getViewgroups <em>Viewgroups</em>}</li>
 *   <li>{@link lab1.impl.ViewModelImpl#getWelcomeViewGroup <em>Welcome View Group</em>}</li>
 *   <li>{@link lab1.impl.ViewModelImpl#getVisibilitycondition <em>Visibilitycondition</em>}</li>
 *   <li>{@link lab1.impl.ViewModelImpl#getDomainmodel <em>Domainmodel</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ViewModelImpl extends MinimalEObjectImpl.Container implements ViewModel {
	/**
	 * The cached value of the '{@link #getViewgroups() <em>Viewgroups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewgroups()
	 * @generated
	 * @ordered
	 */
	protected EList<ViewGroup> viewgroups;

	/**
	 * The cached value of the '{@link #getWelcomeViewGroup() <em>Welcome View Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWelcomeViewGroup()
	 * @generated
	 * @ordered
	 */
	protected ViewGroup welcomeViewGroup;

	/**
	 * The cached value of the '{@link #getVisibilitycondition() <em>Visibilitycondition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibilitycondition()
	 * @generated
	 * @ordered
	 */
	protected EList<VisibilityCondition> visibilitycondition;

	/**
	 * The cached value of the '{@link #getDomainmodel() <em>Domainmodel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainmodel()
	 * @generated
	 * @ordered
	 */
	protected DomainModel domainmodel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ViewModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Lab1Package.Literals.VIEW_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ViewGroup> getViewgroups() {
		if (viewgroups == null) {
			viewgroups = new EObjectContainmentEList<ViewGroup>(ViewGroup.class, this, Lab1Package.VIEW_MODEL__VIEWGROUPS);
		}
		return viewgroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewGroup getWelcomeViewGroup() {
		if (welcomeViewGroup != null && welcomeViewGroup.eIsProxy()) {
			InternalEObject oldWelcomeViewGroup = (InternalEObject)welcomeViewGroup;
			welcomeViewGroup = (ViewGroup)eResolveProxy(oldWelcomeViewGroup);
			if (welcomeViewGroup != oldWelcomeViewGroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Lab1Package.VIEW_MODEL__WELCOME_VIEW_GROUP, oldWelcomeViewGroup, welcomeViewGroup));
			}
		}
		return welcomeViewGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewGroup basicGetWelcomeViewGroup() {
		return welcomeViewGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWelcomeViewGroup(ViewGroup newWelcomeViewGroup) {
		ViewGroup oldWelcomeViewGroup = welcomeViewGroup;
		welcomeViewGroup = newWelcomeViewGroup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VIEW_MODEL__WELCOME_VIEW_GROUP, oldWelcomeViewGroup, welcomeViewGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VisibilityCondition> getVisibilitycondition() {
		if (visibilitycondition == null) {
			visibilitycondition = new EObjectContainmentEList<VisibilityCondition>(VisibilityCondition.class, this, Lab1Package.VIEW_MODEL__VISIBILITYCONDITION);
		}
		return visibilitycondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainModel getDomainmodel() {
		return domainmodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDomainmodel(DomainModel newDomainmodel, NotificationChain msgs) {
		DomainModel oldDomainmodel = domainmodel;
		domainmodel = newDomainmodel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Lab1Package.VIEW_MODEL__DOMAINMODEL, oldDomainmodel, newDomainmodel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomainmodel(DomainModel newDomainmodel) {
		if (newDomainmodel != domainmodel) {
			NotificationChain msgs = null;
			if (domainmodel != null)
				msgs = ((InternalEObject)domainmodel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Lab1Package.VIEW_MODEL__DOMAINMODEL, null, msgs);
			if (newDomainmodel != null)
				msgs = ((InternalEObject)newDomainmodel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Lab1Package.VIEW_MODEL__DOMAINMODEL, null, msgs);
			msgs = basicSetDomainmodel(newDomainmodel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VIEW_MODEL__DOMAINMODEL, newDomainmodel, newDomainmodel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.VIEW_MODEL__VIEWGROUPS:
				return ((InternalEList<?>)getViewgroups()).basicRemove(otherEnd, msgs);
			case Lab1Package.VIEW_MODEL__VISIBILITYCONDITION:
				return ((InternalEList<?>)getVisibilitycondition()).basicRemove(otherEnd, msgs);
			case Lab1Package.VIEW_MODEL__DOMAINMODEL:
				return basicSetDomainmodel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Lab1Package.VIEW_MODEL__VIEWGROUPS:
				return getViewgroups();
			case Lab1Package.VIEW_MODEL__WELCOME_VIEW_GROUP:
				if (resolve) return getWelcomeViewGroup();
				return basicGetWelcomeViewGroup();
			case Lab1Package.VIEW_MODEL__VISIBILITYCONDITION:
				return getVisibilitycondition();
			case Lab1Package.VIEW_MODEL__DOMAINMODEL:
				return getDomainmodel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Lab1Package.VIEW_MODEL__VIEWGROUPS:
				getViewgroups().clear();
				getViewgroups().addAll((Collection<? extends ViewGroup>)newValue);
				return;
			case Lab1Package.VIEW_MODEL__WELCOME_VIEW_GROUP:
				setWelcomeViewGroup((ViewGroup)newValue);
				return;
			case Lab1Package.VIEW_MODEL__VISIBILITYCONDITION:
				getVisibilitycondition().clear();
				getVisibilitycondition().addAll((Collection<? extends VisibilityCondition>)newValue);
				return;
			case Lab1Package.VIEW_MODEL__DOMAINMODEL:
				setDomainmodel((DomainModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Lab1Package.VIEW_MODEL__VIEWGROUPS:
				getViewgroups().clear();
				return;
			case Lab1Package.VIEW_MODEL__WELCOME_VIEW_GROUP:
				setWelcomeViewGroup((ViewGroup)null);
				return;
			case Lab1Package.VIEW_MODEL__VISIBILITYCONDITION:
				getVisibilitycondition().clear();
				return;
			case Lab1Package.VIEW_MODEL__DOMAINMODEL:
				setDomainmodel((DomainModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Lab1Package.VIEW_MODEL__VIEWGROUPS:
				return viewgroups != null && !viewgroups.isEmpty();
			case Lab1Package.VIEW_MODEL__WELCOME_VIEW_GROUP:
				return welcomeViewGroup != null;
			case Lab1Package.VIEW_MODEL__VISIBILITYCONDITION:
				return visibilitycondition != null && !visibilitycondition.isEmpty();
			case Lab1Package.VIEW_MODEL__DOMAINMODEL:
				return domainmodel != null;
		}
		return super.eIsSet(featureID);
	}

} //ViewModelImpl
