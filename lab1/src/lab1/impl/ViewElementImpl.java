/**
 */
package lab1.impl;

import lab1.ClassOperationView;
import lab1.ElementGroup;
import lab1.Lab1Package;
import lab1.ViewElement;
import lab1.VisibilityCondition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>View Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lab1.impl.ViewElementImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link lab1.impl.ViewElementImpl#getElementID <em>Element ID</em>}</li>
 *   <li>{@link lab1.impl.ViewElementImpl#getView <em>View</em>}</li>
 *   <li>{@link lab1.impl.ViewElementImpl#getVisibilitycondition <em>Visibilitycondition</em>}</li>
 *   <li>{@link lab1.impl.ViewElementImpl#getElementgroup <em>Elementgroup</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ViewElementImpl extends MinimalEObjectImpl.Container implements ViewElement {
	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getElementID() <em>Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementID()
	 * @generated
	 * @ordered
	 */
	protected static final int ELEMENT_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getElementID() <em>Element ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementID()
	 * @generated
	 * @ordered
	 */
	protected int elementID = ELEMENT_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getVisibilitycondition() <em>Visibilitycondition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibilitycondition()
	 * @generated
	 * @ordered
	 */
	protected VisibilityCondition visibilitycondition;

	/**
	 * The cached value of the '{@link #getElementgroup() <em>Elementgroup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementgroup()
	 * @generated
	 * @ordered
	 */
	protected ElementGroup elementgroup;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ViewElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Lab1Package.Literals.VIEW_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VIEW_ELEMENT__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getElementID() {
		return elementID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementID(int newElementID) {
		int oldElementID = elementID;
		elementID = newElementID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VIEW_ELEMENT__ELEMENT_ID, oldElementID, elementID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassOperationView getView() {
		if (eContainerFeatureID() != Lab1Package.VIEW_ELEMENT__VIEW) return null;
		return (ClassOperationView)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetView(ClassOperationView newView, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newView, Lab1Package.VIEW_ELEMENT__VIEW, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setView(ClassOperationView newView) {
		if (newView != eInternalContainer() || (eContainerFeatureID() != Lab1Package.VIEW_ELEMENT__VIEW && newView != null)) {
			if (EcoreUtil.isAncestor(this, newView))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newView != null)
				msgs = ((InternalEObject)newView).eInverseAdd(this, Lab1Package.CLASS_OPERATION_VIEW__VIEWELEMENTS, ClassOperationView.class, msgs);
			msgs = basicSetView(newView, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VIEW_ELEMENT__VIEW, newView, newView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementGroup getElementgroup() {
		if (elementgroup != null && elementgroup.eIsProxy()) {
			InternalEObject oldElementgroup = (InternalEObject)elementgroup;
			elementgroup = (ElementGroup)eResolveProxy(oldElementgroup);
			if (elementgroup != oldElementgroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Lab1Package.VIEW_ELEMENT__ELEMENTGROUP, oldElementgroup, elementgroup));
			}
		}
		return elementgroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementGroup basicGetElementgroup() {
		return elementgroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElementgroup(ElementGroup newElementgroup, NotificationChain msgs) {
		ElementGroup oldElementgroup = elementgroup;
		elementgroup = newElementgroup;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Lab1Package.VIEW_ELEMENT__ELEMENTGROUP, oldElementgroup, newElementgroup);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementgroup(ElementGroup newElementgroup) {
		if (newElementgroup != elementgroup) {
			NotificationChain msgs = null;
			if (elementgroup != null)
				msgs = ((InternalEObject)elementgroup).eInverseRemove(this, Lab1Package.ELEMENT_GROUP__VIEWELEMENTS, ElementGroup.class, msgs);
			if (newElementgroup != null)
				msgs = ((InternalEObject)newElementgroup).eInverseAdd(this, Lab1Package.ELEMENT_GROUP__VIEWELEMENTS, ElementGroup.class, msgs);
			msgs = basicSetElementgroup(newElementgroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VIEW_ELEMENT__ELEMENTGROUP, newElementgroup, newElementgroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityCondition getVisibilitycondition() {
		if (visibilitycondition != null && visibilitycondition.eIsProxy()) {
			InternalEObject oldVisibilitycondition = (InternalEObject)visibilitycondition;
			visibilitycondition = (VisibilityCondition)eResolveProxy(oldVisibilitycondition);
			if (visibilitycondition != oldVisibilitycondition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION, oldVisibilitycondition, visibilitycondition));
			}
		}
		return visibilitycondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityCondition basicGetVisibilitycondition() {
		return visibilitycondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVisibilitycondition(VisibilityCondition newVisibilitycondition, NotificationChain msgs) {
		VisibilityCondition oldVisibilitycondition = visibilitycondition;
		visibilitycondition = newVisibilitycondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION, oldVisibilitycondition, newVisibilitycondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisibilitycondition(VisibilityCondition newVisibilitycondition) {
		if (newVisibilitycondition != visibilitycondition) {
			NotificationChain msgs = null;
			if (visibilitycondition != null)
				msgs = ((InternalEObject)visibilitycondition).eInverseRemove(this, Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT, VisibilityCondition.class, msgs);
			if (newVisibilitycondition != null)
				msgs = ((InternalEObject)newVisibilitycondition).eInverseAdd(this, Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT, VisibilityCondition.class, msgs);
			msgs = basicSetVisibilitycondition(newVisibilitycondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION, newVisibilitycondition, newVisibilitycondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.VIEW_ELEMENT__VIEW:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetView((ClassOperationView)otherEnd, msgs);
			case Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION:
				if (visibilitycondition != null)
					msgs = ((InternalEObject)visibilitycondition).eInverseRemove(this, Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT, VisibilityCondition.class, msgs);
				return basicSetVisibilitycondition((VisibilityCondition)otherEnd, msgs);
			case Lab1Package.VIEW_ELEMENT__ELEMENTGROUP:
				if (elementgroup != null)
					msgs = ((InternalEObject)elementgroup).eInverseRemove(this, Lab1Package.ELEMENT_GROUP__VIEWELEMENTS, ElementGroup.class, msgs);
				return basicSetElementgroup((ElementGroup)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.VIEW_ELEMENT__VIEW:
				return basicSetView(null, msgs);
			case Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION:
				return basicSetVisibilitycondition(null, msgs);
			case Lab1Package.VIEW_ELEMENT__ELEMENTGROUP:
				return basicSetElementgroup(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case Lab1Package.VIEW_ELEMENT__VIEW:
				return eInternalContainer().eInverseRemove(this, Lab1Package.CLASS_OPERATION_VIEW__VIEWELEMENTS, ClassOperationView.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Lab1Package.VIEW_ELEMENT__LABEL:
				return getLabel();
			case Lab1Package.VIEW_ELEMENT__ELEMENT_ID:
				return getElementID();
			case Lab1Package.VIEW_ELEMENT__VIEW:
				return getView();
			case Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION:
				if (resolve) return getVisibilitycondition();
				return basicGetVisibilitycondition();
			case Lab1Package.VIEW_ELEMENT__ELEMENTGROUP:
				if (resolve) return getElementgroup();
				return basicGetElementgroup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Lab1Package.VIEW_ELEMENT__LABEL:
				setLabel((String)newValue);
				return;
			case Lab1Package.VIEW_ELEMENT__ELEMENT_ID:
				setElementID((Integer)newValue);
				return;
			case Lab1Package.VIEW_ELEMENT__VIEW:
				setView((ClassOperationView)newValue);
				return;
			case Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION:
				setVisibilitycondition((VisibilityCondition)newValue);
				return;
			case Lab1Package.VIEW_ELEMENT__ELEMENTGROUP:
				setElementgroup((ElementGroup)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Lab1Package.VIEW_ELEMENT__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case Lab1Package.VIEW_ELEMENT__ELEMENT_ID:
				setElementID(ELEMENT_ID_EDEFAULT);
				return;
			case Lab1Package.VIEW_ELEMENT__VIEW:
				setView((ClassOperationView)null);
				return;
			case Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION:
				setVisibilitycondition((VisibilityCondition)null);
				return;
			case Lab1Package.VIEW_ELEMENT__ELEMENTGROUP:
				setElementgroup((ElementGroup)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Lab1Package.VIEW_ELEMENT__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case Lab1Package.VIEW_ELEMENT__ELEMENT_ID:
				return elementID != ELEMENT_ID_EDEFAULT;
			case Lab1Package.VIEW_ELEMENT__VIEW:
				return getView() != null;
			case Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION:
				return visibilitycondition != null;
			case Lab1Package.VIEW_ELEMENT__ELEMENTGROUP:
				return elementgroup != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (label: ");
		result.append(label);
		result.append(", elementID: ");
		result.append(elementID);
		result.append(')');
		return result.toString();
	}

} //ViewElementImpl
