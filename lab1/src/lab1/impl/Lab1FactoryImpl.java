/**
 */
package lab1.impl;

import lab1.Association;
import lab1.AssociationProperty;
import lab1.ClassIndexView;
import lab1.ClassOperationView;
import lab1.ClassProperty;
import lab1.Column;
import lab1.ComparisonCondition;
import lab1.ComparisonConditionOperator;
import lab1.CompositeCondition;
import lab1.CompositeConditionOperator;
import lab1.DataType;
import lab1.DateTimePicker;
import lab1.DomainModel;
import lab1.ElementGroup;
import lab1.EnumerationLiteral;
import lab1.EnumerationLiteralItem;
import lab1.EnumerationType;
import lab1.Lab1Factory;
import lab1.Lab1Package;
import lab1.Layout;
import lab1.Link;
import lab1.List;
import lab1.Selection;
import lab1.SelectionItem;
import lab1.Table;
import lab1.Text;
import lab1.ViewGroup;
import lab1.ViewModel;
import lab1.Visibility;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Lab1FactoryImpl extends EFactoryImpl implements Lab1Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Lab1Factory init() {
		try {
			Lab1Factory theLab1Factory = (Lab1Factory)EPackage.Registry.INSTANCE.getEFactory(Lab1Package.eNS_URI);
			if (theLab1Factory != null) {
				return theLab1Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Lab1FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lab1FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Lab1Package.DOMAIN_MODEL: return createDomainModel();
			case Lab1Package.CLASS: return createClass();
			case Lab1Package.ASSOCIATION: return createAssociation();
			case Lab1Package.DATA_TYPE: return createDataType();
			case Lab1Package.ENUMERATION_LITERAL: return createEnumerationLiteral();
			case Lab1Package.ENUMERATION_TYPE: return createEnumerationType();
			case Lab1Package.CLASS_PROPERTY: return createClassProperty();
			case Lab1Package.ASSOCIATION_PROPERTY: return createAssociationProperty();
			case Lab1Package.DATE_TIME_PICKER: return createDateTimePicker();
			case Lab1Package.VIEW_GROUP: return createViewGroup();
			case Lab1Package.SELECTION: return createSelection();
			case Lab1Package.SELECTION_ITEM: return createSelectionItem();
			case Lab1Package.TEXT: return createText();
			case Lab1Package.CLASS_OPERATION_VIEW: return createClassOperationView();
			case Lab1Package.CLASS_INDEX_VIEW: return createClassIndexView();
			case Lab1Package.ELEMENT_GROUP: return createElementGroup();
			case Lab1Package.VIEW_MODEL: return createViewModel();
			case Lab1Package.LIST: return createList();
			case Lab1Package.TABLE: return createTable();
			case Lab1Package.COLUMN: return createColumn();
			case Lab1Package.COMPARISON_CONDITION: return createComparisonCondition();
			case Lab1Package.COMPOSITE_CONDITION: return createCompositeCondition();
			case Lab1Package.ENUMERATION_LITERAL_ITEM: return createEnumerationLiteralItem();
			case Lab1Package.LINK: return createLink();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case Lab1Package.VISIBILITY:
				return createVisibilityFromString(eDataType, initialValue);
			case Lab1Package.COMPARISON_CONDITION_OPERATOR:
				return createComparisonConditionOperatorFromString(eDataType, initialValue);
			case Lab1Package.COMPOSITE_CONDITION_OPERATOR:
				return createCompositeConditionOperatorFromString(eDataType, initialValue);
			case Lab1Package.LAYOUT:
				return createLayoutFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case Lab1Package.VISIBILITY:
				return convertVisibilityToString(eDataType, instanceValue);
			case Lab1Package.COMPARISON_CONDITION_OPERATOR:
				return convertComparisonConditionOperatorToString(eDataType, instanceValue);
			case Lab1Package.COMPOSITE_CONDITION_OPERATOR:
				return convertCompositeConditionOperatorToString(eDataType, instanceValue);
			case Lab1Package.LAYOUT:
				return convertLayoutToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainModel createDomainModel() {
		DomainModelImpl domainModel = new DomainModelImpl();
		return domainModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public lab1.Class createClass() {
		ClassImpl class_ = new ClassImpl();
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association createAssociation() {
		AssociationImpl association = new AssociationImpl();
		return association;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType createDataType() {
		DataTypeImpl dataType = new DataTypeImpl();
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral createEnumerationLiteral() {
		EnumerationLiteralImpl enumerationLiteral = new EnumerationLiteralImpl();
		return enumerationLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationType createEnumerationType() {
		EnumerationTypeImpl enumerationType = new EnumerationTypeImpl();
		return enumerationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassProperty createClassProperty() {
		ClassPropertyImpl classProperty = new ClassPropertyImpl();
		return classProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationProperty createAssociationProperty() {
		AssociationPropertyImpl associationProperty = new AssociationPropertyImpl();
		return associationProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimePicker createDateTimePicker() {
		DateTimePickerImpl dateTimePicker = new DateTimePickerImpl();
		return dateTimePicker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewGroup createViewGroup() {
		ViewGroupImpl viewGroup = new ViewGroupImpl();
		return viewGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Selection createSelection() {
		SelectionImpl selection = new SelectionImpl();
		return selection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectionItem createSelectionItem() {
		SelectionItemImpl selectionItem = new SelectionItemImpl();
		return selectionItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text createText() {
		TextImpl text = new TextImpl();
		return text;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassOperationView createClassOperationView() {
		ClassOperationViewImpl classOperationView = new ClassOperationViewImpl();
		return classOperationView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassIndexView createClassIndexView() {
		ClassIndexViewImpl classIndexView = new ClassIndexViewImpl();
		return classIndexView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementGroup createElementGroup() {
		ElementGroupImpl elementGroup = new ElementGroupImpl();
		return elementGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewModel createViewModel() {
		ViewModelImpl viewModel = new ViewModelImpl();
		return viewModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List createList() {
		ListImpl list = new ListImpl();
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Table createTable() {
		TableImpl table = new TableImpl();
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Column createColumn() {
		ColumnImpl column = new ColumnImpl();
		return column;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonCondition createComparisonCondition() {
		ComparisonConditionImpl comparisonCondition = new ComparisonConditionImpl();
		return comparisonCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeCondition createCompositeCondition() {
		CompositeConditionImpl compositeCondition = new CompositeConditionImpl();
		return compositeCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteralItem createEnumerationLiteralItem() {
		EnumerationLiteralItemImpl enumerationLiteralItem = new EnumerationLiteralItemImpl();
		return enumerationLiteralItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Link createLink() {
		LinkImpl link = new LinkImpl();
		return link;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Visibility createVisibilityFromString(EDataType eDataType, String initialValue) {
		Visibility result = Visibility.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVisibilityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonConditionOperator createComparisonConditionOperatorFromString(EDataType eDataType, String initialValue) {
		ComparisonConditionOperator result = ComparisonConditionOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertComparisonConditionOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeConditionOperator createCompositeConditionOperatorFromString(EDataType eDataType, String initialValue) {
		CompositeConditionOperator result = CompositeConditionOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCompositeConditionOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Layout createLayoutFromString(EDataType eDataType, String initialValue) {
		Layout result = Layout.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLayoutToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lab1Package getLab1Package() {
		return (Lab1Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Lab1Package getPackage() {
		return Lab1Package.eINSTANCE;
	}

} //Lab1FactoryImpl
