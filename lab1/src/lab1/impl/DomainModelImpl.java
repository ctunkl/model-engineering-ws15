/**
 */
package lab1.impl;

import java.util.Collection;

import lab1.Association;
import lab1.ClassPropertyType;
import lab1.DomainModel;
import lab1.Lab1Package;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Domain Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lab1.impl.DomainModelImpl#getAssociation <em>Association</em>}</li>
 *   <li>{@link lab1.impl.DomainModelImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link lab1.impl.DomainModelImpl#getClasspropertytype <em>Classpropertytype</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DomainModelImpl extends MinimalEObjectImpl.Container implements DomainModel {
	/**
	 * The cached value of the '{@link #getAssociation() <em>Association</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociation()
	 * @generated
	 * @ordered
	 */
	protected EList<Association> association;

	/**
	 * The cached value of the '{@link #getClass_() <em>Class</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected EList<lab1.Class> class_;

	/**
	 * The cached value of the '{@link #getClasspropertytype() <em>Classpropertytype</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClasspropertytype()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassPropertyType> classpropertytype;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DomainModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Lab1Package.Literals.DOMAIN_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Association> getAssociation() {
		if (association == null) {
			association = new EObjectContainmentEList<Association>(Association.class, this, Lab1Package.DOMAIN_MODEL__ASSOCIATION);
		}
		return association;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<lab1.Class> getClass_() {
		if (class_ == null) {
			class_ = new EObjectContainmentEList<lab1.Class>(lab1.Class.class, this, Lab1Package.DOMAIN_MODEL__CLASS);
		}
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassPropertyType> getClasspropertytype() {
		if (classpropertytype == null) {
			classpropertytype = new EObjectContainmentEList<ClassPropertyType>(ClassPropertyType.class, this, Lab1Package.DOMAIN_MODEL__CLASSPROPERTYTYPE);
		}
		return classpropertytype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.DOMAIN_MODEL__ASSOCIATION:
				return ((InternalEList<?>)getAssociation()).basicRemove(otherEnd, msgs);
			case Lab1Package.DOMAIN_MODEL__CLASS:
				return ((InternalEList<?>)getClass_()).basicRemove(otherEnd, msgs);
			case Lab1Package.DOMAIN_MODEL__CLASSPROPERTYTYPE:
				return ((InternalEList<?>)getClasspropertytype()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Lab1Package.DOMAIN_MODEL__ASSOCIATION:
				return getAssociation();
			case Lab1Package.DOMAIN_MODEL__CLASS:
				return getClass_();
			case Lab1Package.DOMAIN_MODEL__CLASSPROPERTYTYPE:
				return getClasspropertytype();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Lab1Package.DOMAIN_MODEL__ASSOCIATION:
				getAssociation().clear();
				getAssociation().addAll((Collection<? extends Association>)newValue);
				return;
			case Lab1Package.DOMAIN_MODEL__CLASS:
				getClass_().clear();
				getClass_().addAll((Collection<? extends lab1.Class>)newValue);
				return;
			case Lab1Package.DOMAIN_MODEL__CLASSPROPERTYTYPE:
				getClasspropertytype().clear();
				getClasspropertytype().addAll((Collection<? extends ClassPropertyType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Lab1Package.DOMAIN_MODEL__ASSOCIATION:
				getAssociation().clear();
				return;
			case Lab1Package.DOMAIN_MODEL__CLASS:
				getClass_().clear();
				return;
			case Lab1Package.DOMAIN_MODEL__CLASSPROPERTYTYPE:
				getClasspropertytype().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Lab1Package.DOMAIN_MODEL__ASSOCIATION:
				return association != null && !association.isEmpty();
			case Lab1Package.DOMAIN_MODEL__CLASS:
				return class_ != null && !class_.isEmpty();
			case Lab1Package.DOMAIN_MODEL__CLASSPROPERTYTYPE:
				return classpropertytype != null && !classpropertytype.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DomainModelImpl
