/**
 */
package lab1.impl;

import java.util.Collection;

import lab1.ClassProperty;
import lab1.ComparisonCondition;
import lab1.Lab1Package;
import lab1.PropertyElement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lab1.impl.PropertyElementImpl#getClassproperty <em>Classproperty</em>}</li>
 *   <li>{@link lab1.impl.PropertyElementImpl#getConditions <em>Conditions</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class PropertyElementImpl extends ViewElementImpl implements PropertyElement {
	/**
	 * The cached value of the '{@link #getClassproperty() <em>Classproperty</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassproperty()
	 * @generated
	 * @ordered
	 */
	protected ClassProperty classproperty;

	/**
	 * The cached value of the '{@link #getConditions() <em>Conditions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<ComparisonCondition> conditions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertyElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Lab1Package.Literals.PROPERTY_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassProperty getClassproperty() {
		if (classproperty != null && classproperty.eIsProxy()) {
			InternalEObject oldClassproperty = (InternalEObject)classproperty;
			classproperty = (ClassProperty)eResolveProxy(oldClassproperty);
			if (classproperty != oldClassproperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Lab1Package.PROPERTY_ELEMENT__CLASSPROPERTY, oldClassproperty, classproperty));
			}
		}
		return classproperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassProperty basicGetClassproperty() {
		return classproperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassproperty(ClassProperty newClassproperty) {
		ClassProperty oldClassproperty = classproperty;
		classproperty = newClassproperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.PROPERTY_ELEMENT__CLASSPROPERTY, oldClassproperty, classproperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComparisonCondition> getConditions() {
		if (conditions == null) {
			conditions = new EObjectWithInverseResolvingEList<ComparisonCondition>(ComparisonCondition.class, this, Lab1Package.PROPERTY_ELEMENT__CONDITIONS, Lab1Package.COMPARISON_CONDITION__PROPERTYELEMENT);
		}
		return conditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.PROPERTY_ELEMENT__CONDITIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConditions()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.PROPERTY_ELEMENT__CONDITIONS:
				return ((InternalEList<?>)getConditions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Lab1Package.PROPERTY_ELEMENT__CLASSPROPERTY:
				if (resolve) return getClassproperty();
				return basicGetClassproperty();
			case Lab1Package.PROPERTY_ELEMENT__CONDITIONS:
				return getConditions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Lab1Package.PROPERTY_ELEMENT__CLASSPROPERTY:
				setClassproperty((ClassProperty)newValue);
				return;
			case Lab1Package.PROPERTY_ELEMENT__CONDITIONS:
				getConditions().clear();
				getConditions().addAll((Collection<? extends ComparisonCondition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Lab1Package.PROPERTY_ELEMENT__CLASSPROPERTY:
				setClassproperty((ClassProperty)null);
				return;
			case Lab1Package.PROPERTY_ELEMENT__CONDITIONS:
				getConditions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Lab1Package.PROPERTY_ELEMENT__CLASSPROPERTY:
				return classproperty != null;
			case Lab1Package.PROPERTY_ELEMENT__CONDITIONS:
				return conditions != null && !conditions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PropertyElementImpl
