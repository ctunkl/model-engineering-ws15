/**
 */
package lab1.impl;

import lab1.EnumerationLiteral;
import lab1.EnumerationLiteralItem;
import lab1.Lab1Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enumeration Literal Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lab1.impl.EnumerationLiteralItemImpl#getEnumerationliteral <em>Enumerationliteral</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnumerationLiteralItemImpl extends SelectionItemImpl implements EnumerationLiteralItem {
	/**
	 * The cached value of the '{@link #getEnumerationliteral() <em>Enumerationliteral</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumerationliteral()
	 * @generated
	 * @ordered
	 */
	protected EnumerationLiteral enumerationliteral;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumerationLiteralItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Lab1Package.Literals.ENUMERATION_LITERAL_ITEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral getEnumerationliteral() {
		if (enumerationliteral != null && enumerationliteral.eIsProxy()) {
			InternalEObject oldEnumerationliteral = (InternalEObject)enumerationliteral;
			enumerationliteral = (EnumerationLiteral)eResolveProxy(oldEnumerationliteral);
			if (enumerationliteral != oldEnumerationliteral) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Lab1Package.ENUMERATION_LITERAL_ITEM__ENUMERATIONLITERAL, oldEnumerationliteral, enumerationliteral));
			}
		}
		return enumerationliteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral basicGetEnumerationliteral() {
		return enumerationliteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerationliteral(EnumerationLiteral newEnumerationliteral) {
		EnumerationLiteral oldEnumerationliteral = enumerationliteral;
		enumerationliteral = newEnumerationliteral;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.ENUMERATION_LITERAL_ITEM__ENUMERATIONLITERAL, oldEnumerationliteral, enumerationliteral));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Lab1Package.ENUMERATION_LITERAL_ITEM__ENUMERATIONLITERAL:
				if (resolve) return getEnumerationliteral();
				return basicGetEnumerationliteral();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Lab1Package.ENUMERATION_LITERAL_ITEM__ENUMERATIONLITERAL:
				setEnumerationliteral((EnumerationLiteral)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Lab1Package.ENUMERATION_LITERAL_ITEM__ENUMERATIONLITERAL:
				setEnumerationliteral((EnumerationLiteral)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Lab1Package.ENUMERATION_LITERAL_ITEM__ENUMERATIONLITERAL:
				return enumerationliteral != null;
		}
		return super.eIsSet(featureID);
	}

} //EnumerationLiteralItemImpl
