/**
 */
package lab1.impl;

import lab1.ComparisonCondition;
import lab1.ComparisonConditionOperator;
import lab1.Lab1Package;
import lab1.PropertyElement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Comparison Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lab1.impl.ComparisonConditionImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link lab1.impl.ComparisonConditionImpl#getValue <em>Value</em>}</li>
 *   <li>{@link lab1.impl.ComparisonConditionImpl#getPropertyelement <em>Propertyelement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComparisonConditionImpl extends VisibilityConditionImpl implements ComparisonCondition {
	/**
	 * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected static final ComparisonConditionOperator OPERATOR_EDEFAULT = ComparisonConditionOperator.EQ;

	/**
	 * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected ComparisonConditionOperator operator = OPERATOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPropertyelement() <em>Propertyelement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyelement()
	 * @generated
	 * @ordered
	 */
	protected PropertyElement propertyelement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComparisonConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Lab1Package.Literals.COMPARISON_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonConditionOperator getOperator() {
		return operator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperator(ComparisonConditionOperator newOperator) {
		ComparisonConditionOperator oldOperator = operator;
		operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.COMPARISON_CONDITION__OPERATOR, oldOperator, operator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.COMPARISON_CONDITION__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyElement getPropertyelement() {
		if (propertyelement != null && propertyelement.eIsProxy()) {
			InternalEObject oldPropertyelement = (InternalEObject)propertyelement;
			propertyelement = (PropertyElement)eResolveProxy(oldPropertyelement);
			if (propertyelement != oldPropertyelement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Lab1Package.COMPARISON_CONDITION__PROPERTYELEMENT, oldPropertyelement, propertyelement));
			}
		}
		return propertyelement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyElement basicGetPropertyelement() {
		return propertyelement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPropertyelement(PropertyElement newPropertyelement, NotificationChain msgs) {
		PropertyElement oldPropertyelement = propertyelement;
		propertyelement = newPropertyelement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Lab1Package.COMPARISON_CONDITION__PROPERTYELEMENT, oldPropertyelement, newPropertyelement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyelement(PropertyElement newPropertyelement) {
		if (newPropertyelement != propertyelement) {
			NotificationChain msgs = null;
			if (propertyelement != null)
				msgs = ((InternalEObject)propertyelement).eInverseRemove(this, Lab1Package.PROPERTY_ELEMENT__CONDITIONS, PropertyElement.class, msgs);
			if (newPropertyelement != null)
				msgs = ((InternalEObject)newPropertyelement).eInverseAdd(this, Lab1Package.PROPERTY_ELEMENT__CONDITIONS, PropertyElement.class, msgs);
			msgs = basicSetPropertyelement(newPropertyelement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.COMPARISON_CONDITION__PROPERTYELEMENT, newPropertyelement, newPropertyelement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.COMPARISON_CONDITION__PROPERTYELEMENT:
				if (propertyelement != null)
					msgs = ((InternalEObject)propertyelement).eInverseRemove(this, Lab1Package.PROPERTY_ELEMENT__CONDITIONS, PropertyElement.class, msgs);
				return basicSetPropertyelement((PropertyElement)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.COMPARISON_CONDITION__PROPERTYELEMENT:
				return basicSetPropertyelement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Lab1Package.COMPARISON_CONDITION__OPERATOR:
				return getOperator();
			case Lab1Package.COMPARISON_CONDITION__VALUE:
				return getValue();
			case Lab1Package.COMPARISON_CONDITION__PROPERTYELEMENT:
				if (resolve) return getPropertyelement();
				return basicGetPropertyelement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Lab1Package.COMPARISON_CONDITION__OPERATOR:
				setOperator((ComparisonConditionOperator)newValue);
				return;
			case Lab1Package.COMPARISON_CONDITION__VALUE:
				setValue((String)newValue);
				return;
			case Lab1Package.COMPARISON_CONDITION__PROPERTYELEMENT:
				setPropertyelement((PropertyElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Lab1Package.COMPARISON_CONDITION__OPERATOR:
				setOperator(OPERATOR_EDEFAULT);
				return;
			case Lab1Package.COMPARISON_CONDITION__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case Lab1Package.COMPARISON_CONDITION__PROPERTYELEMENT:
				setPropertyelement((PropertyElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Lab1Package.COMPARISON_CONDITION__OPERATOR:
				return operator != OPERATOR_EDEFAULT;
			case Lab1Package.COMPARISON_CONDITION__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case Lab1Package.COMPARISON_CONDITION__PROPERTYELEMENT:
				return propertyelement != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (operator: ");
		result.append(operator);
		result.append(", value: ");
		result.append(value);
		result.append(')');
		return result.toString();
	}

} //ComparisonConditionImpl
