/**
 */
package lab1.impl;

import java.util.Collection;

import lab1.CompositeCondition;
import lab1.ElementGroup;
import lab1.Lab1Package;
import lab1.ViewElement;
import lab1.Visibility;
import lab1.VisibilityCondition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Visibility Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lab1.impl.VisibilityConditionImpl#getConditionID <em>Condition ID</em>}</li>
 *   <li>{@link lab1.impl.VisibilityConditionImpl#getVisibilityType <em>Visibility Type</em>}</li>
 *   <li>{@link lab1.impl.VisibilityConditionImpl#getCompositecondition <em>Compositecondition</em>}</li>
 *   <li>{@link lab1.impl.VisibilityConditionImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link lab1.impl.VisibilityConditionImpl#getViewelement <em>Viewelement</em>}</li>
 *   <li>{@link lab1.impl.VisibilityConditionImpl#getElementgroup <em>Elementgroup</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class VisibilityConditionImpl extends MinimalEObjectImpl.Container implements VisibilityCondition {
	/**
	 * The default value of the '{@link #getConditionID() <em>Condition ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionID()
	 * @generated
	 * @ordered
	 */
	protected static final int CONDITION_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getConditionID() <em>Condition ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionID()
	 * @generated
	 * @ordered
	 */
	protected int conditionID = CONDITION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVisibilityType() <em>Visibility Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibilityType()
	 * @generated
	 * @ordered
	 */
	protected static final Visibility VISIBILITY_TYPE_EDEFAULT = Visibility.SHOWN;

	/**
	 * The cached value of the '{@link #getVisibilityType() <em>Visibility Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibilityType()
	 * @generated
	 * @ordered
	 */
	protected Visibility visibilityType = VISIBILITY_TYPE_EDEFAULT;

	/**
	 * The cached setting delegate for the '{@link #getChildren() <em>Children</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate CHILDREN__ESETTING_DELEGATE = ((EStructuralFeature.Internal)Lab1Package.Literals.VISIBILITY_CONDITION__CHILDREN).getSettingDelegate();

	/**
	 * The cached value of the '{@link #getViewelement() <em>Viewelement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewelement()
	 * @generated
	 * @ordered
	 */
	protected ViewElement viewelement;

	/**
	 * The cached value of the '{@link #getElementgroup() <em>Elementgroup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementgroup()
	 * @generated
	 * @ordered
	 */
	protected ElementGroup elementgroup;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisibilityConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Lab1Package.Literals.VISIBILITY_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getConditionID() {
		return conditionID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditionID(int newConditionID) {
		int oldConditionID = conditionID;
		conditionID = newConditionID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VISIBILITY_CONDITION__CONDITION_ID, oldConditionID, conditionID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Visibility getVisibilityType() {
		return visibilityType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisibilityType(Visibility newVisibilityType) {
		Visibility oldVisibilityType = visibilityType;
		visibilityType = newVisibilityType == null ? VISIBILITY_TYPE_EDEFAULT : newVisibilityType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VISIBILITY_CONDITION__VISIBILITY_TYPE, oldVisibilityType, visibilityType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeCondition getCompositecondition() {
		if (eContainerFeatureID() != Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION) return null;
		return (CompositeCondition)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCompositecondition(CompositeCondition newCompositecondition, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCompositecondition, Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompositecondition(CompositeCondition newCompositecondition) {
		if (newCompositecondition != eInternalContainer() || (eContainerFeatureID() != Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION && newCompositecondition != null)) {
			if (EcoreUtil.isAncestor(this, newCompositecondition))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCompositecondition != null)
				msgs = ((InternalEObject)newCompositecondition).eInverseAdd(this, Lab1Package.COMPOSITE_CONDITION__OPERANDS, CompositeCondition.class, msgs);
			msgs = basicSetCompositecondition(newCompositecondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION, newCompositecondition, newCompositecondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<VisibilityCondition> getChildren() {
		return (EList<VisibilityCondition>)CHILDREN__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewElement getViewelement() {
		if (viewelement != null && viewelement.eIsProxy()) {
			InternalEObject oldViewelement = (InternalEObject)viewelement;
			viewelement = (ViewElement)eResolveProxy(oldViewelement);
			if (viewelement != oldViewelement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT, oldViewelement, viewelement));
			}
		}
		return viewelement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewElement basicGetViewelement() {
		return viewelement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetViewelement(ViewElement newViewelement, NotificationChain msgs) {
		ViewElement oldViewelement = viewelement;
		viewelement = newViewelement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT, oldViewelement, newViewelement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setViewelement(ViewElement newViewelement) {
		if (newViewelement != viewelement) {
			NotificationChain msgs = null;
			if (viewelement != null)
				msgs = ((InternalEObject)viewelement).eInverseRemove(this, Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION, ViewElement.class, msgs);
			if (newViewelement != null)
				msgs = ((InternalEObject)newViewelement).eInverseAdd(this, Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION, ViewElement.class, msgs);
			msgs = basicSetViewelement(newViewelement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT, newViewelement, newViewelement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementGroup getElementgroup() {
		if (elementgroup != null && elementgroup.eIsProxy()) {
			InternalEObject oldElementgroup = (InternalEObject)elementgroup;
			elementgroup = (ElementGroup)eResolveProxy(oldElementgroup);
			if (elementgroup != oldElementgroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP, oldElementgroup, elementgroup));
			}
		}
		return elementgroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementGroup basicGetElementgroup() {
		return elementgroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElementgroup(ElementGroup newElementgroup, NotificationChain msgs) {
		ElementGroup oldElementgroup = elementgroup;
		elementgroup = newElementgroup;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP, oldElementgroup, newElementgroup);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementgroup(ElementGroup newElementgroup) {
		if (newElementgroup != elementgroup) {
			NotificationChain msgs = null;
			if (elementgroup != null)
				msgs = ((InternalEObject)elementgroup).eInverseRemove(this, Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION, ElementGroup.class, msgs);
			if (newElementgroup != null)
				msgs = ((InternalEObject)newElementgroup).eInverseAdd(this, Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION, ElementGroup.class, msgs);
			msgs = basicSetElementgroup(newElementgroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP, newElementgroup, newElementgroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCompositecondition((CompositeCondition)otherEnd, msgs);
			case Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT:
				if (viewelement != null)
					msgs = ((InternalEObject)viewelement).eInverseRemove(this, Lab1Package.VIEW_ELEMENT__VISIBILITYCONDITION, ViewElement.class, msgs);
				return basicSetViewelement((ViewElement)otherEnd, msgs);
			case Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP:
				if (elementgroup != null)
					msgs = ((InternalEObject)elementgroup).eInverseRemove(this, Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION, ElementGroup.class, msgs);
				return basicSetElementgroup((ElementGroup)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION:
				return basicSetCompositecondition(null, msgs);
			case Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT:
				return basicSetViewelement(null, msgs);
			case Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP:
				return basicSetElementgroup(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION:
				return eInternalContainer().eInverseRemove(this, Lab1Package.COMPOSITE_CONDITION__OPERANDS, CompositeCondition.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Lab1Package.VISIBILITY_CONDITION__CONDITION_ID:
				return getConditionID();
			case Lab1Package.VISIBILITY_CONDITION__VISIBILITY_TYPE:
				return getVisibilityType();
			case Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION:
				return getCompositecondition();
			case Lab1Package.VISIBILITY_CONDITION__CHILDREN:
				return getChildren();
			case Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT:
				if (resolve) return getViewelement();
				return basicGetViewelement();
			case Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP:
				if (resolve) return getElementgroup();
				return basicGetElementgroup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Lab1Package.VISIBILITY_CONDITION__CONDITION_ID:
				setConditionID((Integer)newValue);
				return;
			case Lab1Package.VISIBILITY_CONDITION__VISIBILITY_TYPE:
				setVisibilityType((Visibility)newValue);
				return;
			case Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION:
				setCompositecondition((CompositeCondition)newValue);
				return;
			case Lab1Package.VISIBILITY_CONDITION__CHILDREN:
				getChildren().clear();
				getChildren().addAll((Collection<? extends VisibilityCondition>)newValue);
				return;
			case Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT:
				setViewelement((ViewElement)newValue);
				return;
			case Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP:
				setElementgroup((ElementGroup)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Lab1Package.VISIBILITY_CONDITION__CONDITION_ID:
				setConditionID(CONDITION_ID_EDEFAULT);
				return;
			case Lab1Package.VISIBILITY_CONDITION__VISIBILITY_TYPE:
				setVisibilityType(VISIBILITY_TYPE_EDEFAULT);
				return;
			case Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION:
				setCompositecondition((CompositeCondition)null);
				return;
			case Lab1Package.VISIBILITY_CONDITION__CHILDREN:
				getChildren().clear();
				return;
			case Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT:
				setViewelement((ViewElement)null);
				return;
			case Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP:
				setElementgroup((ElementGroup)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Lab1Package.VISIBILITY_CONDITION__CONDITION_ID:
				return conditionID != CONDITION_ID_EDEFAULT;
			case Lab1Package.VISIBILITY_CONDITION__VISIBILITY_TYPE:
				return visibilityType != VISIBILITY_TYPE_EDEFAULT;
			case Lab1Package.VISIBILITY_CONDITION__COMPOSITECONDITION:
				return getCompositecondition() != null;
			case Lab1Package.VISIBILITY_CONDITION__CHILDREN:
				return CHILDREN__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case Lab1Package.VISIBILITY_CONDITION__VIEWELEMENT:
				return viewelement != null;
			case Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP:
				return elementgroup != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (conditionID: ");
		result.append(conditionID);
		result.append(", visibilityType: ");
		result.append(visibilityType);
		result.append(')');
		return result.toString();
	}

} //VisibilityConditionImpl
