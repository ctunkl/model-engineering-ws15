/**
 */
package lab1.impl;

import java.util.Collection;
import lab1.ElementGroup;
import lab1.Lab1Package;
import lab1.Layout;
import lab1.ViewElement;
import lab1.VisibilityCondition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lab1.impl.ElementGroupImpl#getHeader <em>Header</em>}</li>
 *   <li>{@link lab1.impl.ElementGroupImpl#getLayout <em>Layout</em>}</li>
 *   <li>{@link lab1.impl.ElementGroupImpl#getVisibilitycondition <em>Visibilitycondition</em>}</li>
 *   <li>{@link lab1.impl.ElementGroupImpl#getViewelements <em>Viewelements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ElementGroupImpl extends MinimalEObjectImpl.Container implements ElementGroup {
	/**
	 * The default value of the '{@link #getHeader() <em>Header</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeader()
	 * @generated
	 * @ordered
	 */
	protected static final String HEADER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHeader() <em>Header</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeader()
	 * @generated
	 * @ordered
	 */
	protected String header = HEADER_EDEFAULT;

	/**
	 * The default value of the '{@link #getLayout() <em>Layout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayout()
	 * @generated
	 * @ordered
	 */
	protected static final Layout LAYOUT_EDEFAULT = Layout.VERTICAL;

	/**
	 * The cached value of the '{@link #getLayout() <em>Layout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayout()
	 * @generated
	 * @ordered
	 */
	protected Layout layout = LAYOUT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getVisibilitycondition() <em>Visibilitycondition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibilitycondition()
	 * @generated
	 * @ordered
	 */
	protected VisibilityCondition visibilitycondition;

	/**
	 * The cached value of the '{@link #getViewelements() <em>Viewelements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewelements()
	 * @generated
	 * @ordered
	 */
	protected EList<ViewElement> viewelements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Lab1Package.Literals.ELEMENT_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeader(String newHeader) {
		String oldHeader = header;
		header = newHeader;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.ELEMENT_GROUP__HEADER, oldHeader, header));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Layout getLayout() {
		return layout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLayout(Layout newLayout) {
		Layout oldLayout = layout;
		layout = newLayout == null ? LAYOUT_EDEFAULT : newLayout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.ELEMENT_GROUP__LAYOUT, oldLayout, layout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityCondition getVisibilitycondition() {
		if (visibilitycondition != null && visibilitycondition.eIsProxy()) {
			InternalEObject oldVisibilitycondition = (InternalEObject)visibilitycondition;
			visibilitycondition = (VisibilityCondition)eResolveProxy(oldVisibilitycondition);
			if (visibilitycondition != oldVisibilitycondition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION, oldVisibilitycondition, visibilitycondition));
			}
		}
		return visibilitycondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityCondition basicGetVisibilitycondition() {
		return visibilitycondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVisibilitycondition(VisibilityCondition newVisibilitycondition, NotificationChain msgs) {
		VisibilityCondition oldVisibilitycondition = visibilitycondition;
		visibilitycondition = newVisibilitycondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION, oldVisibilitycondition, newVisibilitycondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisibilitycondition(VisibilityCondition newVisibilitycondition) {
		if (newVisibilitycondition != visibilitycondition) {
			NotificationChain msgs = null;
			if (visibilitycondition != null)
				msgs = ((InternalEObject)visibilitycondition).eInverseRemove(this, Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP, VisibilityCondition.class, msgs);
			if (newVisibilitycondition != null)
				msgs = ((InternalEObject)newVisibilitycondition).eInverseAdd(this, Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP, VisibilityCondition.class, msgs);
			msgs = basicSetVisibilitycondition(newVisibilitycondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION, newVisibilitycondition, newVisibilitycondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ViewElement> getViewelements() {
		if (viewelements == null) {
			viewelements = new EObjectWithInverseResolvingEList<ViewElement>(ViewElement.class, this, Lab1Package.ELEMENT_GROUP__VIEWELEMENTS, Lab1Package.VIEW_ELEMENT__ELEMENTGROUP);
		}
		return viewelements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION:
				if (visibilitycondition != null)
					msgs = ((InternalEObject)visibilitycondition).eInverseRemove(this, Lab1Package.VISIBILITY_CONDITION__ELEMENTGROUP, VisibilityCondition.class, msgs);
				return basicSetVisibilitycondition((VisibilityCondition)otherEnd, msgs);
			case Lab1Package.ELEMENT_GROUP__VIEWELEMENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getViewelements()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION:
				return basicSetVisibilitycondition(null, msgs);
			case Lab1Package.ELEMENT_GROUP__VIEWELEMENTS:
				return ((InternalEList<?>)getViewelements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Lab1Package.ELEMENT_GROUP__HEADER:
				return getHeader();
			case Lab1Package.ELEMENT_GROUP__LAYOUT:
				return getLayout();
			case Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION:
				if (resolve) return getVisibilitycondition();
				return basicGetVisibilitycondition();
			case Lab1Package.ELEMENT_GROUP__VIEWELEMENTS:
				return getViewelements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Lab1Package.ELEMENT_GROUP__HEADER:
				setHeader((String)newValue);
				return;
			case Lab1Package.ELEMENT_GROUP__LAYOUT:
				setLayout((Layout)newValue);
				return;
			case Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION:
				setVisibilitycondition((VisibilityCondition)newValue);
				return;
			case Lab1Package.ELEMENT_GROUP__VIEWELEMENTS:
				getViewelements().clear();
				getViewelements().addAll((Collection<? extends ViewElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Lab1Package.ELEMENT_GROUP__HEADER:
				setHeader(HEADER_EDEFAULT);
				return;
			case Lab1Package.ELEMENT_GROUP__LAYOUT:
				setLayout(LAYOUT_EDEFAULT);
				return;
			case Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION:
				setVisibilitycondition((VisibilityCondition)null);
				return;
			case Lab1Package.ELEMENT_GROUP__VIEWELEMENTS:
				getViewelements().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Lab1Package.ELEMENT_GROUP__HEADER:
				return HEADER_EDEFAULT == null ? header != null : !HEADER_EDEFAULT.equals(header);
			case Lab1Package.ELEMENT_GROUP__LAYOUT:
				return layout != LAYOUT_EDEFAULT;
			case Lab1Package.ELEMENT_GROUP__VISIBILITYCONDITION:
				return visibilitycondition != null;
			case Lab1Package.ELEMENT_GROUP__VIEWELEMENTS:
				return viewelements != null && !viewelements.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (header: ");
		result.append(header);
		result.append(", layout: ");
		result.append(layout);
		result.append(')');
		return result.toString();
	}

} //ElementGroupImpl
