/**
 */
package lab1.impl;

import java.util.Collection;

import lab1.ClassOperationView;
import lab1.ElementGroup;
import lab1.Lab1Package;
import lab1.Layout;

import lab1.ViewElement;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Operation View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link lab1.impl.ClassOperationViewImpl#getLayout <em>Layout</em>}</li>
 *   <li>{@link lab1.impl.ClassOperationViewImpl#getViewelements <em>Viewelements</em>}</li>
 *   <li>{@link lab1.impl.ClassOperationViewImpl#getElementgroups <em>Elementgroups</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassOperationViewImpl extends ViewImpl implements ClassOperationView {
	/**
	 * The default value of the '{@link #getLayout() <em>Layout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayout()
	 * @generated
	 * @ordered
	 */
	protected static final Layout LAYOUT_EDEFAULT = Layout.VERTICAL;

	/**
	 * The cached value of the '{@link #getLayout() <em>Layout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayout()
	 * @generated
	 * @ordered
	 */
	protected Layout layout = LAYOUT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getViewelements() <em>Viewelements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewelements()
	 * @generated
	 * @ordered
	 */
	protected EList<ViewElement> viewelements;

	/**
	 * The cached value of the '{@link #getElementgroups() <em>Elementgroups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementgroups()
	 * @generated
	 * @ordered
	 */
	protected EList<ElementGroup> elementgroups;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassOperationViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Lab1Package.Literals.CLASS_OPERATION_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Layout getLayout() {
		return layout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLayout(Layout newLayout) {
		Layout oldLayout = layout;
		layout = newLayout == null ? LAYOUT_EDEFAULT : newLayout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Lab1Package.CLASS_OPERATION_VIEW__LAYOUT, oldLayout, layout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ViewElement> getViewelements() {
		if (viewelements == null) {
			viewelements = new EObjectContainmentWithInverseEList<ViewElement>(ViewElement.class, this, Lab1Package.CLASS_OPERATION_VIEW__VIEWELEMENTS, Lab1Package.VIEW_ELEMENT__VIEW);
		}
		return viewelements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElementGroup> getElementgroups() {
		if (elementgroups == null) {
			elementgroups = new EObjectContainmentEList<ElementGroup>(ElementGroup.class, this, Lab1Package.CLASS_OPERATION_VIEW__ELEMENTGROUPS);
		}
		return elementgroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.CLASS_OPERATION_VIEW__VIEWELEMENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getViewelements()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Lab1Package.CLASS_OPERATION_VIEW__VIEWELEMENTS:
				return ((InternalEList<?>)getViewelements()).basicRemove(otherEnd, msgs);
			case Lab1Package.CLASS_OPERATION_VIEW__ELEMENTGROUPS:
				return ((InternalEList<?>)getElementgroups()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Lab1Package.CLASS_OPERATION_VIEW__LAYOUT:
				return getLayout();
			case Lab1Package.CLASS_OPERATION_VIEW__VIEWELEMENTS:
				return getViewelements();
			case Lab1Package.CLASS_OPERATION_VIEW__ELEMENTGROUPS:
				return getElementgroups();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Lab1Package.CLASS_OPERATION_VIEW__LAYOUT:
				setLayout((Layout)newValue);
				return;
			case Lab1Package.CLASS_OPERATION_VIEW__VIEWELEMENTS:
				getViewelements().clear();
				getViewelements().addAll((Collection<? extends ViewElement>)newValue);
				return;
			case Lab1Package.CLASS_OPERATION_VIEW__ELEMENTGROUPS:
				getElementgroups().clear();
				getElementgroups().addAll((Collection<? extends ElementGroup>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Lab1Package.CLASS_OPERATION_VIEW__LAYOUT:
				setLayout(LAYOUT_EDEFAULT);
				return;
			case Lab1Package.CLASS_OPERATION_VIEW__VIEWELEMENTS:
				getViewelements().clear();
				return;
			case Lab1Package.CLASS_OPERATION_VIEW__ELEMENTGROUPS:
				getElementgroups().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Lab1Package.CLASS_OPERATION_VIEW__LAYOUT:
				return layout != LAYOUT_EDEFAULT;
			case Lab1Package.CLASS_OPERATION_VIEW__VIEWELEMENTS:
				return viewelements != null && !viewelements.isEmpty();
			case Lab1Package.CLASS_OPERATION_VIEW__ELEMENTGROUPS:
				return elementgroups != null && !elementgroups.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (layout: ");
		result.append(layout);
		result.append(')');
		return result.toString();
	}

} //ClassOperationViewImpl
