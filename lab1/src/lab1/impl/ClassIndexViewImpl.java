/**
 */
package lab1.impl;

import lab1.ClassIndexView;
import lab1.Lab1Package;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Index View</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClassIndexViewImpl extends ViewImpl implements ClassIndexView {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassIndexViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Lab1Package.Literals.CLASS_INDEX_VIEW;
	}

} //ClassIndexViewImpl
