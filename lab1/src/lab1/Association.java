/**
 */
package lab1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.Association#isIsComposition <em>Is Composition</em>}</li>
 *   <li>{@link lab1.Association#getAssociationProperties <em>Association Properties</em>}</li>
 *   <li>{@link lab1.Association#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getAssociation()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='exactlyOneNavigableEnd'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot exactlyOneNavigableEnd='self.associationProperties -> select(ap: AssociationProperty | ap.isNavigable) -> size() = 1'"
 * @generated
 */
public interface Association extends EObject {
	/**
	 * Returns the value of the '<em><b>Is Composition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Composition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Composition</em>' attribute.
	 * @see #setIsComposition(boolean)
	 * @see lab1.Lab1Package#getAssociation_IsComposition()
	 * @model
	 * @generated
	 */
	boolean isIsComposition();

	/**
	 * Sets the value of the '{@link lab1.Association#isIsComposition <em>Is Composition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Composition</em>' attribute.
	 * @see #isIsComposition()
	 * @generated
	 */
	void setIsComposition(boolean value);

	/**
	 * Returns the value of the '<em><b>Association Properties</b></em>' containment reference list.
	 * The list contents are of type {@link lab1.AssociationProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association Properties</em>' containment reference list.
	 * @see lab1.Lab1Package#getAssociation_AssociationProperties()
	 * @model containment="true" lower="2" upper="2"
	 * @generated
	 */
	EList<AssociationProperty> getAssociationProperties();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see lab1.Lab1Package#getAssociation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link lab1.Association#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Association
