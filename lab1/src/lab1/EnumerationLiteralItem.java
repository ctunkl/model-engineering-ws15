/**
 */
package lab1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumeration Literal Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link lab1.EnumerationLiteralItem#getEnumerationliteral <em>Enumerationliteral</em>}</li>
 * </ul>
 *
 * @see lab1.Lab1Package#getEnumerationLiteralItem()
 * @model
 * @generated
 */
public interface EnumerationLiteralItem extends SelectionItem {

	/**
	 * Returns the value of the '<em><b>Enumerationliteral</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumerationliteral</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumerationliteral</em>' reference.
	 * @see #setEnumerationliteral(EnumerationLiteral)
	 * @see lab1.Lab1Package#getEnumerationLiteralItem_Enumerationliteral()
	 * @model required="true"
	 * @generated
	 */
	EnumerationLiteral getEnumerationliteral();

	/**
	 * Sets the value of the '{@link lab1.EnumerationLiteralItem#getEnumerationliteral <em>Enumerationliteral</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumerationliteral</em>' reference.
	 * @see #getEnumerationliteral()
	 * @generated
	 */
	void setEnumerationliteral(EnumerationLiteral value);
} // EnumerationLiteralItem
