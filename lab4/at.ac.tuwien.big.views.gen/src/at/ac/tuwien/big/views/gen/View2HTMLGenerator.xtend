package at.ac.tuwien.big.views.gen

import at.ac.tuwien.big.views.Class
import at.ac.tuwien.big.views.ClassIndexView
import at.ac.tuwien.big.views.ClassOperationView
import at.ac.tuwien.big.views.Column
import at.ac.tuwien.big.views.ComparisonCondition
import at.ac.tuwien.big.views.ComparisonConditionType
import at.ac.tuwien.big.views.CompositeCondition
import at.ac.tuwien.big.views.CompositeConditionType
import at.ac.tuwien.big.views.ConditionalElement
import at.ac.tuwien.big.views.CreateView
import at.ac.tuwien.big.views.DateTimePicker
import at.ac.tuwien.big.views.DeleteView
import at.ac.tuwien.big.views.ElementGroup
import at.ac.tuwien.big.views.EnumerationLiteralItem
import at.ac.tuwien.big.views.LayoutStyle
import at.ac.tuwien.big.views.Link
import at.ac.tuwien.big.views.LinkableElement
import at.ac.tuwien.big.views.List
import at.ac.tuwien.big.views.PropertyElement
import at.ac.tuwien.big.views.ReadView
import at.ac.tuwien.big.views.Selection
import at.ac.tuwien.big.views.SelectionItem
import at.ac.tuwien.big.views.Table
import at.ac.tuwien.big.views.Text
import at.ac.tuwien.big.views.UpdateView
import at.ac.tuwien.big.views.View
import at.ac.tuwien.big.views.ViewElement
import at.ac.tuwien.big.views.ViewGroup
import at.ac.tuwien.big.views.ViewModel
import at.ac.tuwien.big.views.VisibilityCondition
import java.io.File
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator

class View2HTMLGenerator implements IGenerator {

	override doGenerate(Resource resource, IFileSystemAccess fsa) {
		if (resource.contents.get(0) instanceof ViewModel) {
			var viewModel = resource.contents.get(0) as ViewModel
			val htmlFileName = new File(getWelcomeGroup(viewModel).name.name+".html");
			fsa.generateFile(
				htmlFileName.toString,'''
					<!DOCTYPE html>
					«var welcomegroup_name = getWelcomeGroup(viewModel).name.name»
					<html lang="en" data-ng-app="«welcomegroup_name»App">
					«generateHead(viewModel)»
					<body data-ng-controller="«welcomegroup_name»Controller">

					«generateNavBar(viewModel)»
					«generateViews(viewModel)»

					</body>
					</html>'''
			)
		}
	}

	def generateHead(ViewModel viewModel) { '''
		<head>
			<title>Views</title>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<script src="../assets/moment-with-locales.js"></script>
			<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
			<script src="../assets/datetimepickerDirective.js"></script>
			<script src="../assets/views.js"></script>
			<script src="«getWelcomeGroup(viewModel).name».js"></script>
			<script type="text/javascript">
					$(document).ready(
						function(){
							view.addWelcomePage('«getWelcomeGroupStartView(viewModel).name.strip»');
							view.init();
					});
			</script>
		</head>'''
	}

	def getWelcomeGroup(ViewModel model) {
		for(group : model.viewGroups)
			if(group.isWelcomeViewGroup)
				return group
	}

	def getWelcomeGroupStartView(ViewModel model) {
		for(view : getWelcomeGroup(model).views)
			if(view.startView)
				return view
	}

	def getStartViewForViewGroup(ViewGroup vg) {
		for(view : vg.views)
			if(view.startView)
				return view
	}

	def generateNavBar(ViewModel model) { '''
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div>
					<ul class="nav navbar-nav">
						«FOR ViewGroup vg : model.viewGroups»
							<li><a href="" class="viewgroup" target="«getStartViewForViewGroup(vg).name.strip»">«vg.name»</a></li>
						«ENDFOR»
					</ul>
				</div>
			</div>
		</nav>'''
	}

	def generateViews(ViewModel model) { '''
		«FOR ViewGroup vg : model.viewGroups»
			«generateViews(vg)»
		«ENDFOR»'''
	}

	def generateViews(ViewGroup vg) { '''
		«FOR View v : vg.views»
			«generateView(v)»
		«ENDFOR»'''
	}

	def generateView(View v) {
		switch v {
			case v instanceof ClassIndexView: generateIndexView(v as ClassIndexView)
			case v instanceof ReadView || v instanceof DeleteView: generateReadDeleteView(v as ClassOperationView)
			case v instanceof CreateView || v instanceof UpdateView: generateCreateUpdateView(v as ClassOperationView)
		}
	}

	def generateIndexView(ClassIndexView v) {
		val lclass = v.class_.name.toLowerCase

	'''
		<div class="container" id="«v.name.strip»">
			<h2>«v.header»</h2>
			<h3>«v.description»</h3>
			<ul>
				<li data-ng-repeat="«lclass» in «lclass»s"> {{ «lclass».«v.class_.id.name» }}
					«generateLinks(v)»
				</li>
			    «generateAddButton(v)»
			</ul>
		</div>'''
	}

	def generateLinks(LinkableElement e) {
		'''
		«FOR Link l : e.link»
			«generateLink(l)»
		«ENDFOR»'''
	}

	def generateLink(Link l) {
		generateLinkForView(l.targetView)
	}

	def dispatch generateLinkForView(View v) {
		// no output for view subtypes without dedicated dispatch method
	}

	def dispatch generateLinkForView(ReadView v) {
		val clazzName = v.class_.name

		'''
			<a href="" data-toggle="modal" data-target="#modal«v.name.strip»"
			           data-ng-click="get«clazzName»(«clazzName.toLowerCase».id)">show</a>'''
	}

	def dispatch generateLinkForView(DeleteView v) {
		val clazzName = v.class_.name

		'''
			<a href="" data-toggle="modal" data-target="#modal«v.name.strip»"
			           data-ng-click="get«clazzName»(«clazzName.toLowerCase».id)">delete</a>
		'''
	}

	def dispatch generateLinkForView(UpdateView v) {
		val clazzName = v.class_.name

		'''
			<a href="" data-ng-click="navigation«v.header»('«v.name.strip»'); update«clazzName»(«clazzName.toLowerCase».id)">update</a>
		'''
	}

	def generateReadDeleteView(ClassOperationView v) { '''
		<div class="modal fade" id="modal«v.name.strip»">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">«v.header»</h4>
					</div>
					<div class="modal-body">
						<p>«v.description»</p>
						<h5>«v.name»</h5>

						«FOR ElementGroup e : v.elementGroups»
							«FOR ViewElement ve : e.viewElements»
								«IF ve instanceof PropertyElement»
									«val pe = ve as PropertyElement»
									<p>«pe.label»: {{ «(pe.property.eContainer as Class).name.name».«pe.property.name» }}</p>
								«ENDIF»
							«ENDFOR»
						«ENDFOR»

					</div>
					<div class="modal-footer">
						«generateModalButtons(v)»
					</div>
				</div>
			</div>
		</div>'''
	}

	def generateCreateUpdateView(ClassOperationView v) { 
		val vg = v.eContainer as ViewGroup

	'''
		<div class="container" id="«v.name.strip»">
			<h2>«v.header»</h2>
			<form name="«v.name.strip»Form" novalidate>
				<p>«v.description»</p>
				<div class="panel-group">
					«IF v.layout.alignment == LayoutStyle.HORIZONTAL»
						<div class="row">
					«ENDIF»

					«FOR ElementGroup e : v.elementGroups»
						«generateElementGroup(e)»
					«ENDFOR»

					«IF v.layout.alignment == LayoutStyle.HORIZONTAL»
						</div>
					«ENDIF»
				</div>
				
				«IF !(vg.isWelcomeViewGroup && v.startView)»
					«generateSaveButton(v)»
				«ENDIF»
			</form>
		</div>'''
	}
	
	def generateSaveButton(ClassOperationView v) {
		val viewModel = (v.eContainer as ViewGroup).eContainer as ViewModel
		val welcomeStartView = getWelcomeGroupStartView(viewModel)
		
		'''
		<button value="«welcomeStartView.name.strip»" class="btn btn-primary btn-sm" 
		        data-ng-disabled="«v.name.strip»Form.$invalid" 
		        data-ng-click="save«v.class_.name»()">Save</button>'''
	}

	def dispatch generateModalButtons(ReadView v) {
		'''<button class="btn btn-default" data-dismiss="modal">Close</button>'''
	}
	
	def dispatch generateModalButtons(DeleteView v) {
		'''
		<button class="btn btn-default" data-dismiss="modal" 
		        data-ng-click="delete«v.class_.name»(«v.class_.name.toLowerCase».id)">Delete</button>
		<button class="btn btn-default" data-dismiss="modal">Cancel</button>'''
	}

	def generateElementGroup(ElementGroup e) {
		val viewAlignment = (e.eContainer as ClassOperationView).layout.alignment
		val groupAlignment = e.layout.alignment

		'''
		«IF viewAlignment == LayoutStyle.VERTICAL»
			<div class="elementgroup" «generateCondition(e)»>
		«ENDIF»

		«IF viewAlignment == LayoutStyle.HORIZONTAL»
			<div class="elementgroup col-sm-6" «generateCondition(e)»>
		«ENDIF»

				<h4>«e.header»</h4>
				<div class="panel-body">
					«IF groupAlignment == LayoutStyle.HORIZONTAL»
						<div class="form-inline" role="form">
					«ENDIF»

					«FOR ViewElement ve : e.viewElements»
						«generateViewElement(ve)»
					«ENDFOR»

					«IF groupAlignment == LayoutStyle.HORIZONTAL»
						</div>
					«ENDIF»
				</div>
			</div>'''
	}

	def dispatch generateViewElement(Text p) {
		val prop = p.property
		val clazz = ((p.eContainer as ElementGroup).eContainer as View).class_
		val req = prop.lowerBound == 1 && prop.upperBound == 1

		'''
		<div class="form-group">
			«generateLabel(p)»

			«IF p.long»
				<textarea class="form-control" rows="4" id="«p.elementID»" name="«prop.name»"
					data-ng-model="new«clazz.name.name».«prop.name»" «if (req) "required"» «generateCondition(p)»>
				</textarea>
			«ELSE»
				<input type="text" class="form-control" id="«p.elementID»" name="«prop.name»"
					data-ng-model="new«clazz.name.name».«prop.name»" «if (req) "required"»
					data-ng-pattern="/«p.format»/" «generateCondition(p)» />
			«ENDIF»

			«generateErrorSpanTags(p)»
		</div>'''
	}

	def generateLabel(PropertyElement p) {
		val prop = p.property
		val req = prop.lowerBound == 1 && prop.upperBound == 1

		'''<label for="«p.elementID»">«p.label»«if (req) "<span>*</span>"»:</label>'''
	}

	def dispatch generateViewElement(Selection sel) {
		val prop = sel.property
		val clazz = prop.eContainer as Class
		val req = prop.lowerBound == 1 && prop.upperBound == 1

		'''
		<div class="form-group">
			«generateLabel(sel)»

			<select data-ng-option class="form-control" id="«sel.elementID»"
			        data-ng-model="new«clazz.name.name».«prop.name»" «if (req) "required"» «generateCondition(sel)»>

				<option value="" disabled selected>Select your option</option>

				«FOR SelectionItem item : sel.selectionItems»
					«generateSelectionItem(item)»
				«ENDFOR»
			</select>

			«generateErrorSpanTags(sel)»
		</div>'''
	}

	def dispatch generateSelectionItem(SelectionItem item) {
		'''<option value="«item.value»">«item.value»</option>'''
	}

	def dispatch generateSelectionItem(EnumerationLiteralItem item) {
		'''<option value="«item.enumerationLiteral.value»">«item.enumerationLiteral.name»</option>'''
	}

	def dispatch generateViewElement(DateTimePicker picker) {
		val prop = picker.property
		val clazz = prop.eContainer as Class
		val type = picker.property.type.name

		var String style = null

		switch type {
			case "Date": style = 'calendar'
			case "Time": style = 'time'
		}

		'''
		<div class="form-group">
			<div class="row">
				<div class="col-xs-6 col-sm-12">
					<div class="form-group">

						«generateLabel(picker)»
						<div class="input-group date" id="picker«picker.elementID»" style="«style»">
							<input type="text" class="form-control" id="«picker.elementID»" name="«prop.name»"
								   data-ng-model="new«clazz.name.name».«prop.name»" data-ng-pattern="/«picker.format»/"
		                           «generateCondition(picker)» />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-«style»"></span>
							</span>
						</div>
					</div>
				</div>
			</div>

			«generateErrorSpanTags(picker)»
		</div>'''
	}

	def generateErrorSpanTags(PropertyElement p) {
		var viewName = (p.eContainer.eContainer as View).name.strip
		var label = p.label.normalize
		val req = p.property.lowerBound == 1 && p.property.upperBound == 1

		'''
		<span class="«viewName»Span" style="color:red"
		      data-ng-show="«viewName»Form.«label».$dirty && «viewName»Form.«label».$invalid">

			«IF req»
				<span data-ng-show="«viewName»Form.«label».$error.required">Input is mandatory.</span>
			«ENDIF»

			«IF requiresPatternWarning(p)»
				<span data-ng-show="«viewName»Form.«label».$error.pattern">Input doesn't match expected pattern.</span>
			«ENDIF»
		</span>'''
	}

	def normalize(String label) {
		label.replaceAll("[^a-zA-Z0-9]", "").toLowerCase
	}

	def dispatch requiresPatternWarning(PropertyElement p) {
		false
	}

	def dispatch requiresPatternWarning(Text t) {
		t.format != null
	}

	def dispatch requiresPatternWarning(DateTimePicker picker) {
		picker.format != null
	}

	def dispatch generateViewElement(List list) {
		val clazz = list.association.navigableEnd.type as Class
		val clazzName = clazz.name.name

	'''
		<div class="form-group">
			<div «generateCondition(list)»>
				<h5>«list.label»</h5>
				<ul id="«list.elementID»">
					<li data-ng-repeat="«clazzName» in «clazzName»s"> {{ «clazzName».«clazz.id.name» }}
						«generateLinks(list)»
					</li>
				</ul>
				«generateAddButton(list)»
			</div>
		</div>'''
	}

	def generateAddButton(LinkableElement e) {
		for(Link link : e.link) {
			if(link.targetView instanceof CreateView) {
				return '''<button value="«link.targetView.name.strip»" class="btn btn-primary btn-sm">Add</button>'''	
			}
		}
	}

	def generateCondition(ConditionalElement e) {
		if(e.condition instanceof VisibilityCondition) {
			val cond = e.condition as VisibilityCondition

			'''«attributeNameForCondition(cond)»="«generateVisibilityConditionString(e, cond)»"'''
		}
	}

	def attributeNameForCondition(VisibilityCondition c) {
		switch(c.type) {
			case ENABLE: "data-ng-enabled"
			case DISABLE: "data-ng-disabled"
			case SHOW: "data-ng-show"
			case HIDE: "data-ng-hide"
		}
	}

	def dispatch generateVisibilityConditionString(ConditionalElement e, ComparisonCondition cond) {
		var String clazzName

		if(e.eContainer instanceof View) {
			clazzName = (e.eContainer as View).class_.name
		} else {
			clazzName = (e.eContainer.eContainer as View).class_.name
		}

		val propName = cond.property.property.name
		val comparisonType = generateComparisonOperator(cond.comparisonType)
		val value = cond.comparisonValue

		'''new«clazzName.toLowerCase».«propName» «comparisonType» '«value»' '''
	}

	def dispatch generateVisibilityConditionString(ConditionalElement e, CompositeCondition cond) {
		val cond1 = generateVisibilityConditionString(e, cond.composedConditions.get(0))
		val cond2 = generateVisibilityConditionString(e, cond.composedConditions.get(1))

		'''(«cond1» «generateCompositionOperator(cond.compositionType)» «cond2»)'''
	}

	def generateComparisonOperator(ComparisonConditionType type) {
		switch(type) {
			case EQUAL: "=="
			case LESS: "<"
			case GREATER: ">"
		}
	}

	def generateCompositionOperator(CompositeConditionType type) {
		switch(type) {
			case AND: "&&"
			case OR: "||"
		}
	}

	def dispatch generateViewElement(Table table) {
		val clazz = table.association.navigableEnd.type as Class
		val clazzName = clazz.name.name

		'''
		<div class="form-group">
			<div «generateCondition(table)»>
				<h5>«table.label»</h5>
				<table class="table table-striped" id="«table.elementID»">
					<thead>
						<tr>
							«FOR Column col : table.columns»
								<th>«col.label»</th>
							«ENDFOR»
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr data-ng-repeat="«clazzName» in «clazzName»s">
							«FOR Column col : table.columns»
								<td> {{ «clazzName».«col.property.name» }} </td>
							«ENDFOR»
							<td>«generateLinks(table)»</td>
						</tr>
					</tbody>
				</table>
				«generateAddButton(table)»
			</div>
		</div>'''
	}	

	def getName(String st){
		return st.toLowerCase.strip
	}

	def strip(String st){
		return st.replaceAll("\\W", "")
	}
}