package at.ac.tuwien.big.views.gen

import java.io.File
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.ac.tuwien.big.views.DomainModel
import at.ac.tuwien.big.views.Class

class Domain2AngularJSGenerator implements IGenerator {
	
	override doGenerate(Resource resource, IFileSystemAccess fsa) {
		if (resource.contents.get(0) instanceof DomainModel) {
			var domainModel = resource.contents.get(0) as DomainModel
			val angularJSFileName = new File(getFirstClassName(domainModel).toLowerCase+".js");
			fsa.generateFile(
				angularJSFileName.toString,
				'''
				«generateModule(domainModel)»
				«generateService(domainModel)»
				«generateController(domainModel)»'''
			)
		}
	}
	
	def generateModule(DomainModel model) {
		'''var module = angular.module('«getFirstClassName(model)»App', []);'''
	}
	
	def generateService(DomainModel model) { '''
		module.service('«getFirstClassName(model)»Service', function () {
			
		«FOR Class c : model.domainModelElements.filter(Class)»
			«val lname = c.name.toLowerCase»
			
			var «lname»s = [];
			var «lname»id = 0;
			
			this.save«c.name»Service = function («lname») {
				if («lname».id == null) {
					«lname».id = «lname»id++;
					«lname»s.push(«lname»);
				} else {
					for (i in «lname»s) {
						if («lname»s[i].id == «lname».id) {
						   	«lname»s[i] = «lname»;
						}
					}
				}
			}

			this.get«c.name»Service = function (id) {
				for (i in «lname»s) {
					if («lname»s[i].id == id) {
						return «lname»s[i];
					}   
				}
			}
		
			this.delete«c.name»Service = function (id) {
				for (i in «lname»s) {
					if («lname»s[i].id == id) {
						«lname»s.splice(i, 1);
					}
				}
			}
		
			this.list«c.name»Service = function () {
				return «lname»s;
			}
		«ENDFOR»
		});'''
	}
	
	def generateController(DomainModel model) {	'''
		module.controller('«getFirstClassName(model)»Controller', function ($scope, «getFirstClassName(model)»Service) {

		«FOR Class c : model.domainModelElements.filter(Class)»
			«val lname = c.name.toLowerCase»
	
			$scope.«lname»s = «getFirstClassName(model)»Service.list«c.name»Service();
					
			$scope.save«c.name» = function () {
				«getFirstClassName(model)»Service.save«c.name»Service($scope.new«lname»);
				$scope.new«lname» = {};
			}
		
			$scope.delete«c.name» = function (id) {
				«getFirstClassName(model)»Service.delete«c.name»Service(id);
			}
		
			$scope.update«c.name» = function (id) {
				$scope.new«lname» = angular.copy(«getFirstClassName(model)»Service.get«c.name»Service(id));
			}
		
			$scope.get«c.name» = function (id) {
				$scope.«lname» = angular.copy(«getFirstClassName(model)»Service.get«c.name»Service(id));
			}
					
			$scope.navigation«c.name» = function (targetView) {
				$(".container").hide(); 
				var view = angular.element('#'+targetView);
				view.show();
			}
		«ENDFOR»
		});'''
	}
	
	def getFirstClassName(DomainModel model) {
		var classname = model.domainModelElements.filter(Class).get(0)
		return classname.name.toLowerCase.replaceAll("\\W", "");
	}	
}