/**
 */
package lab1.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import lab1.ElementGroup;
import lab1.Lab1Factory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Element Group</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ElementGroupTest extends TestCase {

	/**
	 * The fixture for this Element Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementGroup fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ElementGroupTest.class);
	}

	/**
	 * Constructs a new Element Group test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementGroupTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Element Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ElementGroup fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Element Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementGroup getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Lab1Factory.eINSTANCE.createElementGroup());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ElementGroupTest
