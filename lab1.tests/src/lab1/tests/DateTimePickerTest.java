/**
 */
package lab1.tests;

import junit.textui.TestRunner;

import lab1.DateTimePicker;
import lab1.Lab1Factory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Date Time Picker</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DateTimePickerTest extends PropertyElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DateTimePickerTest.class);
	}

	/**
	 * Constructs a new Date Time Picker test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimePickerTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Date Time Picker test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DateTimePicker getFixture() {
		return (DateTimePicker)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Lab1Factory.eINSTANCE.createDateTimePicker());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DateTimePickerTest
