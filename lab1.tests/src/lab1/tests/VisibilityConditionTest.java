/**
 */
package lab1.tests;

import junit.framework.TestCase;

import lab1.VisibilityCondition;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Visibility Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link lab1.VisibilityCondition#getChildren() <em>Children</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class VisibilityConditionTest extends TestCase {

	/**
	 * The fixture for this Visibility Condition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisibilityCondition fixture = null;

	/**
	 * Constructs a new Visibility Condition test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityConditionTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Visibility Condition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(VisibilityCondition fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Visibility Condition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisibilityCondition getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link lab1.VisibilityCondition#getChildren() <em>Children</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see lab1.VisibilityCondition#getChildren()
	 * @generated
	 */
	public void testGetChildren() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //VisibilityConditionTest
