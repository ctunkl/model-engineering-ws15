/**
 */
package lab1.tests;

import junit.textui.TestRunner;

import lab1.AssociationProperty;
import lab1.Lab1Factory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Association Property</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class AssociationPropertyTest extends PropertyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(AssociationPropertyTest.class);
	}

	/**
	 * Constructs a new Association Property test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationPropertyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Association Property test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AssociationProperty getFixture() {
		return (AssociationProperty)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Lab1Factory.eINSTANCE.createAssociationProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //AssociationPropertyTest
