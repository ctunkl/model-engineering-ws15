/**
 */
package lab1.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>lab1</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class Lab1Tests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new Lab1Tests("lab1 Tests");
		suite.addTestSuite(ClassTest.class);
		suite.addTestSuite(ComparisonConditionTest.class);
		suite.addTestSuite(CompositeConditionTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lab1Tests(String name) {
		super(name);
	}

} //Lab1Tests
