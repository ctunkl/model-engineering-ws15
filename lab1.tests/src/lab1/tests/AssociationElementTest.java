/**
 */
package lab1.tests;

import lab1.AssociationElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Association Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AssociationElementTest extends ViewElementTest {

	/**
	 * Constructs a new Association Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Association Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AssociationElement getFixture() {
		return (AssociationElement)fixture;
	}

} //AssociationElementTest
