/**
 */
package lab1.tests;

import junit.textui.TestRunner;

import lab1.ClassProperty;
import lab1.Lab1Factory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Class Property</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassPropertyTest extends PropertyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ClassPropertyTest.class);
	}

	/**
	 * Constructs a new Class Property test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassPropertyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Class Property test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ClassProperty getFixture() {
		return (ClassProperty)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Lab1Factory.eINSTANCE.createClassProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ClassPropertyTest
