/**
 */
package lab1.tests;

import junit.framework.TestCase;

import lab1.ClassPropertyType;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Class Property Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ClassPropertyTypeTest extends TestCase {

	/**
	 * The fixture for this Class Property Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassPropertyType fixture = null;

	/**
	 * Constructs a new Class Property Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassPropertyTypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Class Property Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ClassPropertyType fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Class Property Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassPropertyType getFixture() {
		return fixture;
	}

} //ClassPropertyTypeTest
